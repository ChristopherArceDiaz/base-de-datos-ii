<?php
    // $datoUsario = $_GET['datosPersona'];
    // $datousuario = unserialize($_REQUEST["datosPersona"]);


    // $idPersona = $_GET['idPersona'];
    // $primerApellido = $_GET['primerApellido'];
    // $primerNombre = $_GET['primerNombre'];

    
    // $_SESSION['idPersona'] = $idPersona;
    // $_SESSION['primerNombre'] = $primerNombre;
    // $_SESSION['primerApellid'] = $primerApellido;

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>Inicio</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
</head>
<body>
<?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 10) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button" style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i> 
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
 
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                         <li><a href="IUListaPersonalTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona2.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                   <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:   <?php 
                        //   echo $_REQUEST['primerNombre']." ".$_REQUEST['primerApellido'];
                      
                        // session_start();
                        // echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        // session_start();
                        
                        echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
           

                        
                        ?></span>
                </li>
                <li  class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom" title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li  class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom" title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Sistema bibliotecario <small>Inicio</small></h1>
            </div>
        </div>
        <section class="full-reset text-center" style="padding: 40px 0;">
        <img src="../img/Administración.jpg" class="img-fluid" alt="Responsive image" style="width: 80%; height: 300px;">
    
        
        </section>
        
        <?php
           include 'footer.php'
           ?>
</body>
</html>