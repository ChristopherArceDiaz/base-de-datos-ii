<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/Persona.php");
        require_once("../Model/Rol.php");
        require_once("../Model/Carrera.php");
        
        $objPersona = new Persona();
        $listaDePersonas = $objPersona->listaDePersonas();
        
        $objetoRol = new Rol();
        $listaRol = $objetoRol->listaRol();


        $objetoCarrera = new Carrera();
        // $listaCarreraRegistro = $objetoCarrera->listaCarreraRegistro($_REQUEST['infor']);
        $listaCarrera = $objetoCarrera->listaCarrera();

        // $objetoAsignacionCarrera = $_REQUEST['idUltimo'];

        
        // $objetoAsignacionCarrera = new AsignacionCarrera();


        // $registrarAsignacionCarrera = $objetoAsignacionCarrera->registrarAsignacionCarrera();

        date_default_timezone_set('America/La_Paz');  
        $fechaActual = date('Y-m-d H:i:s');
    // exit;

        // $datosPersona = unserialize($_GET["datosPersona"]);

?>


<!DOCTYPE html>
<html lang="es">

<head>
    <title>Registrar Tesis</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- <link rel="stylesheet" href="../css/form.css"> -->


    <!-- <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" href="../css/formulario.css"> -->
    <!-- <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script> -->
</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>

            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                         <li><a href="IUListaPersonalTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona2.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador: <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?></span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Añadir Usuario</small></h1>
            </div>
        </div>
        <div class="container-fluid" style="margin: 50px 0;">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <img src="../assets/img/user01.png" alt="pdf" class="img-responsive center-box"
                        style="max-width: 110px;">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Bienvenido a la sección para agregar nueva persona a la biblioteca, deberas de llenar todos los
                    campos para poder registrar la persona
                </div>
            </div>
        </div>
        <div class="container-fluid">


            <form autocomplete="off" method="post" action="../Controller/LNRegistrarPersona.php"
                enctype="multipart/form-data" class="needs-validation" novalidate>
                <div class="container-flat-form">
                    <div class="title-flat-form title-flat-blue">Nuevo Usuario</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">

                            <div class="group-material">
                                <span>Rol</span>
                                <select name="idRol" id="idRol" class="tooltips-general material-control"
                                    data-toggle="tooltip" data-placement="top" title="Elige la Tipo de Tesis del libro">
                                    <option value="" disabled="" selected="">Selecciona un Tipo de Rol</option>
                                    <?php foreach($listaRol as $rol){ ?>
                                    <option value='<?php echo $rol['idRol'];?>'><?php echo $rol['nombre'];?></option>
                                    <?php }?>
                                </select>
                            </div>


                            <!-- <div class="group-material">
                                <span>Carrera</span>
                                <select name="idCarrera" class="tooltips-general material-control" data-toggle="tooltip"
                                    data-placement="top" title="Elige la carrera de la Tesis">
                                    <option value="" disabled="" selected="">Selecciona una Carrera</option>
                                    <?php foreach($listaCarrera as $carrera){ ?>
                                    <option value='<?php echo $carrera['idCarrera'];?>'><?php echo $carrera['nombre'];?>
                                    </option>
                                    <?php }?> 
                                </select>
                            </div> -->


                            <div class="group-material">
                                <input name="ci" id="ci" type="text" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu ci" data-toggle="tooltip" data-placement="top"
                                    title="Escribe tu numero de ci, solamente números" pattern="[0-9]{7,8}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>CI</label>
                            </div>
                            <div class="group-material">
                                <input name="primerNombre" id="primerNombre" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu primer Nombre" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Apellido Paterno, solamente letras"
                                    pattern="[A-Za-z]{3,20}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Primer Nombre</label>
                            </div>
                            <div class="group-material">
                                <input name="segundoNombre" id="segundoNombre" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Segundo Nombre, solamente letras"
                                    pattern="[A-Za-z]{3,20}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Segundo Nombre</label>
                            </div>
                            <div class="group-material">
                                <input name="primerApellido" id="primerApellido" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Apellido Paterno, solamente letras"
                                    pattern="[A-Za-z]{3,20}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Apellido Paterno</label>
                            </div>
                            <div class="group-material">
                                <input name="segundoApellido" id="segundoApellido" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Apellido Materno, solamente letras"
                                    pattern="[A-Za-z]{3,20}">

                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Apellido Materno</label>
                            </div>
                            <div class="group-material">
                                <input name="telefono" id="telefono" type="text"
                                    class="tooltips-general material-control" placeholder="Escribe aquí tu telefono"
                                    data-toggle="tooltip" data-placement="top"
                                    title="Escribe tu numero de telefono, solamente números" pattern="[0-9]{5,8}">

                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Telefono</label>
                            </div>
                            <div class="group-material">

                                <input name="file" id="file" type="file" class="tooltips-general material-control">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Fotografia</label>
                            </div>
                            <div class="group-material">
                                <input name="fechaRegistro" id="fechaRegistro" type="hidden"
                                    class="tooltips-general material-control" value="<?= $fechaActual?>">
                                <span class="highlight"></span>
                                <span class="bar"></span>

                            </div>

                            <div class="group-material">
                                <input name="fechaActualizacion" id="fechaActualizacion" type="hidden"
                                    class="tooltips-general material-control" value="<?= $fechaActual?>">
                                <span class="highlight"></span>
                                <span class="bar"></span>

                            </div>


                            <p class="text-center">
                                <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i
                                        class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                <button id="enviar" type="submit" class="btn btn-primary"><i
                                        class="zmdi zmdi-floppy"></i>
                                    &nbsp;&nbsp; Guardar</button>
                            </p>
                        </div>
                    </div>
                </div>
            </form>

        </div>

        <!-- VALIDACION 
            <main>
                <form action="../Controller/LNRegistrarPersona.php" method="get" class="formulario" id="formulario" enctype="multipart/form-data">

                    <div class="group-material">
                        <span>Rol</span>
                        <select name="idRol" id="idRol" class="tooltips-general material-control" data-toggle="tooltip"
                            data-placement="top" title="Selecciona un Tipo de Rol">
                            <option value="" disabled="" selected="">Selecciona un Tipo de Rol</option>
                            <?php foreach($listaRol as $rol){ ?>
                            <option value='<?php echo $rol['idRol'];?>'><?php echo $rol['nombre'];?></option>
                            <?php }?>
                        </select>
                    </div>
                   
                    <div class="formulario__grupo" id="grupo__ci">
                        <label for="ci" class="formulario__label">Cedula de Identidad</label>
                        <div class="formulario__grupo-input">
                            <input type="text" class="formulario__input" name="ci" id="ci"
                                placeholder="john123">
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">El usuario tiene que ser de 4 a 16 dígitos y solo puede
                            contener numeros, letras y guion bajo.</p>
                    </div>

                    <div class="formulario__grupo" id="grupo__primerNombre">
                        <label for="nombre" class="formulario__label">Primer Nombre</label>
                        <div class="formulario__grupo-input">
                            <input type="text" class="formulario__input" name="primerNombre" id="primerNombre"
                                placeholder="John Doe">
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">El usuario tiene que ser de 4 a 16 dígitos y solo puede
                            contener numeros, letras y guion bajo.</p>
                    </div>

                    <div class="formulario__grupo" id="grupo__segundoNombre">
                        <label for="nombre" class="formulario__label">Segundo Nombre</label>
                        <div class="formulario__grupo-input">
                            <input type="text" class="formulario__input" name="segundoNombre" id="segundoNombre"
                                placeholder="John Doe">
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">El usuario tiene que ser de 4 a 16 dígitos y solo puede
                            contener numeros, letras y guion bajo.</p>
                    </div>

                    <div class="formulario__grupo" id="grupo__primerApellido">
                        <label for="nombre" class="formulario__label">Apellido Paterno</label>
                        <div class="formulario__grupo-input">
                            <input type="text" class="formulario__input" name="primerApellido" id="primerApellido"
                                placeholder="John Doe">
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">El usuario tiene que ser de 4 a 16 dígitos y solo puede
                            contener numeros, letras y guion bajo.</p>
                    </div>

                    <div class="formulario__grupo" id="grupo__segundoApellido">
                        <label for="nombre" class="formulario__label">Apellido Materno</label>
                        <div class="formulario__grupo-input">
                            <input type="text" class="formulario__input" name="segundoApellido" id="segundoApellido"
                                placeholder="John Doe">
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">El usuario tiene que ser de 4 a 16 dígitos y solo puede
                            contener numeros, letras y guion bajo.</p>
                    </div>


                    <div class="formulario__grupo" id="grupo__telefono">
                        <label for="telefono" class="formulario__label">Teléfono</label>
                        <div class="formulario__grupo-input">
                            <input type="text" class="formulario__input" name="telefono" id="telefono"
                                placeholder="4491234567">
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">El telefono solo puede contener numeros y el maximo son 14
                            dígitos.</p>
                    </div>

            
                    <div class="formulario__grupo" id="grupo__fotografia">
                        <label for="fotografia" class="formulario__label">Fotografia</label>
                        <div class="formulario__grupo-input">
                            <input type="file" class="formulario__input" name="file" id="file">
                            <i class="formulario__validacion-estado fas fa-times-circle"></i>
                        </div>
                        <p class="formulario__input-error">El correo solo puede contener letras, numeros, puntos,
                            guiones y
                            guion bajo.</p>
                    </div>

                    <div class="group-material">
                        <input name="fechaRegistro" id="fechaRegistro" type="hidden"
                            class="tooltips-general material-control" value="<?= $fechaActual?>">
                        <span class="highlight"></span>
                        <span class="bar"></span>

                    </div>

                    <div class="group-material">
                        <input name="fechaActualizacion" id="fechaActualizacion" type="hidden"
                            class="tooltips-general material-control" value="<?= $fechaActual?>">
                        <span class="highlight"></span>
                        <span class="bar"></span>

                    </div>

                    <div class="formulario__grupo" id="grupo__terminos">
                        <label class="formulario__label">
                            <input class="formulario__checkbox" type="checkbox" name="terminos" id="terminos">
                            Acepto los Terminos y Condiciones
                        </label>
                    </div>

                    <div class="formulario__mensaje" id="formulario__mensaje">
                        <p><i class="fas fa-exclamation-triangle"></i> <b>Error:</b> Por favor rellena el formulario
                            correctamente. </p>
                    </div>

                    <div class="formulario__grupo formulario__grupo-btn-enviar">
                        <button type="submit" class="formulario__btn">Enviar</button>
                        <p class="formulario__mensaje-exito" id="formulario__mensaje-exito">Formulario enviado
                            exitosamente!</p>
                    </div>
                </form>
            </main> -->



        <?php
           include 'footer.php';
           ?>


    <!-- <script src="../js/popper.min.js"></script>	 -->
        <script src="../js/validaciones.js"></script>
       
        <!-- <script src="../js/formulario.js"></script> -->
        <!-- <script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script> -->
        <!-- <script src="../js/ci.js"></script> -->
</body>

</html>