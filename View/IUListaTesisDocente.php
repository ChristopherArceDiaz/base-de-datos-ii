<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/DocumentoTesis.php");
        require_once("../Model/TipoTesis.php");
        require_once("../Model/Facultad.php");
        require_once("../Model/Carrera.php");
        
        $objDocumentoTesis = new DocumentoTesis();

        $listaAnioTesis = $objDocumentoTesis->listaAnioTesis();

        // $listaDocumentoTesis = $objDocumentoTesis->listaDocumentoTesis();

        $objetoTipoTesis = new TipoTesis();
        $listaTipoTesis = $objetoTipoTesis->listaTipoTesis();


        $objetoFacultad = new Facultad();

        $existeDatosFacultad = $objetoFacultad->listaFacultad();

        $objetoCarrera = new Carrera();

        $listaCarrera = $objetoCarrera->listaCarrera();
        // exit;
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Docente</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/texto.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>


</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>
                <!-- <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p> -->
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="IUPrincipalDocente.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Inicio</a></li>
                    <li><a href="IUPerfilDocente.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i
                                class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                    <li><a href="IUListaTesisDocente.php"><i
                                class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Tesis</a></li>


                    <!-- <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li> -->
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Docente:
                        <?php 
                        echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?>
                    </span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Catálogo de libros</small></h1>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Puedes buscar una tesis por Autor, Título o Tipo de Tesis
                </div>
            </div>
        </div>

        <h2>Filtro de Busquedas</h2>
        <!-- <form class="form-inline"> -->
        <div class="form-group" style="width:800px; margin:0 auto;">
            <label for="busqueda">Busqueda General: </label>
            <input type="text" name="busqueda" id="busqueda" style="width:400px">
        </div>
        <br>
        <div class="form-inline">
            <div class="form-group">
                <label for="facultad">Facultad: </label>
                <select class="form-control" id="facultad" name="facultad" style="width:290px"
                    onchange="ShowFacultad();">
                    <option value="0">Seleciona facultad</option>
                    <?php
        foreach($existeDatosFacultad as $facultad){ ?>

                    <option value='<?php echo $facultad['idFacultad'];?>'><?php echo $facultad['nombre'];?></o>

                        <?php
         }
         ?>
                </select>
            </div>
            <div class="form-group">
                <label for="carrera">Carrera: </label>
                <select class="form-control" id="carrera" name="carrera" style="width:260px" onchange="ShowCarrera();">
                    <option value="0">Seleciona Carrera</option>
                    <?php
        foreach($listaCarrera as $carrera){ ?>

                    <option value='<?php echo $carrera['idFacultad'];?>'><?php echo $carrera['nombre'];?></option>

                    <?php
         }
         ?>
                </select>
            </div>
            <div class="form-group">
                <label for="tipoTesis">Tipo de Tesis: </label>
                <select class="form-control" id="tipoTesis" name="tipoTesis" style="width:130px"
                    onchange="ShowTipoTesis();">
                    <option value="0">Tipo de Tesis</option>
                    <?php foreach($listaTipoTesis as $tipoTesis){ ?>
                    <option value='<?php echo $tipoTesis['idTipoTesis'];?>'><?php echo $tipoTesis['nombre'];?></option>
                    <?php }?>
                </select>
            </div>
            <div class="form-group">
                <label for="tipoTesis">Año: </label>
                <select class="form-control" id="anio" name="anio" style="width:130px" onchange="ShowAnio();">
                    <option value="0">Año:</option>
                    <?php foreach($listaAnioTesis as $AnioTesis){ ?>
                    <option value='<?php echo $AnioTesis['Anio'];?>'><?php echo $AnioTesis['Anio'];?></option>
                    <?php }?>
                </select>
            </div>

        </div>

        <br>

        <!-- IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->
        <table class="table table-bordered table-sm" id="busquedaTitulo">
            <thead>
                <tr>
                    <td>#</td>
                    <td WIDTH="150">Codigo</td>
                    <td WIDTH="200">Autor</td>
                    <td WIDTH="650">Titulo</td>
                    <td WIDTH="150">Tipo de Bibliografia</td>

                </tr>
            </thead>
            <tbody id="tBodyData">


            </tbody>

        </table>






        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                    </div>
                    <div class="modal-body">
                        En caso de tener algun problema, comunicarse a: christopherarce2357@gmail.com
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Proyecto para la materia de Base de Datos II.
                            <br> Docente: Ing. Maria Hurtado
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Christopher Arce Diaz <i
                                    class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i
                                    class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2020 Christopher Arce Diaz</div>
        </footer>
    </div>



    <!-- <script src="../js/busqueda.js"></script> -->
    <!-- <script type="text/javascript" src="../js/busquedaUsuarioExterno.js"></script> -->
    <script type="text/javascript" src="../js/filtroBusquedasDocente.js"></script>
</body>

</html>