<?php
    require_once("../Model/Conexion.php");
    require_once("../Model/DocumentoTesis.php");
    require_once("../Model/TipoTesis.php");

    $objetoDocumentoTesis = new DocumentoTesis();
    // $objetoDocumentoTesis = new LNListaTesis();

    $existeDatosTesis = $objetoDocumentoTesis->informacionDocumentoTesisTitulo($_REQUEST['infor']);

    $objetoTipoTesis = new TipoTesis();
    $listaTipoTesis = $objetoTipoTesis->listaTipoTesis();
    // $idDocumentoTesis = $_POST['idDocumentoTesis'];

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Catálogo</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <!-- <link rel="stylesheet" href="../css/EstiloTexto.css"> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>


</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>
                <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p>
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                    <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i
                                class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                    <li><a href="IUListaPersona.php"><i
                                class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                    <li><a href="IUListaTesisAdmi.php"><i
                                class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                    <li><a href="IUListaPersonalTesisAdmi.php"><i
                                class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a>
                    </li>

                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="IURegistrarTesis.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp;
                                    Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona.php"><i
                                        class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Reportes y estadísticas</a></li>

                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:
                        <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?></span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Informacion de la Tesis</small></h1>

            </div>
        </div>

        <form action="../Controller/LNActualizarTesis.php" method="post">
            <div class="container">
                <br>
                <div class="row">
                    <div class="cold-md-5">

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="seccion1">

                                <div class="card bg-info" style="width: 60rem;">

                                    <?php
                              
                                       
                        
                                    foreach($existeDatosTesis as $informacionTesis ):  ?>
                                    <input type="hidden" name="idDocumentoTesis" id="idDocumentoTesis"
                                        value="<?php echo $informacionTesis['idDocumentoTesis'] ?>">
                                    <div class="group-material">
                                        <span>Tipo de Tesis</span>
                                        <select name="idTipoTesis" class="tooltips-general material-control"
                                            data-toggle="tooltip" data-placement="top"
                                            title="Elige la Tipo de Tesis del libro">
                                            <option value="" disabled="" selected="">
                                                <?php echo $informacionTesis['TipoBibliografia'] ?>
                                            </option>
                                            <?php foreach($listaTipoTesis as $tipoTesis){ ?>
                                            <option value='<?php echo $tipoTesis['idTipoTesis'];?>'>
                                                <?php echo $tipoTesis['nombre'];?></option>
                                            <?php }?>
                                        </select>
                                    </div>


                                    <div class="group-material">
                                        <input name="titulo" type="text" class="tooltips-general material-control"
                                            placeholder="Escribe aquí el título de la Tesis" data-toggle="tooltip"
                                            data-placement="top" title="Escribe el título de la Tesis"
                                            value="<?php echo $informacionTesis['Titulo'] ?>">

                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Título</label>
                                    </div>

                                    <div class="group-material">
                                        <h4>Resumen</h4>
                                        <textarea name="resumen" rows="40"
                                            cols="150"><?php echo $informacionTesis['Resumen'] ?></textarea>

                                        <!-- <input name="resumen" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el Resumen de la Tesis" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Escribe el Resumen de la Tesis"> -->
                                        <!-- <span class="highlight"></span>
                                    <span class="bar"></span> -->

                                    </div>
                                    <div class="group-material">
                                        <h4>Introduccion</h4>


                                        <textarea name="introduccion" rows="20"
                                            cols="150"><?php echo $informacionTesis['Introduccion'] ?></textarea>
                                        <!-- <input name="introduccion" type="text" class="tooltips-general material-control" placeholder="Escribe aquí la Introducción de la Tesis" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Escribe la Introducción de la Tesis"> -->
                                        <!-- <span class="highlight"></span>
                                    <span class="bar"></span> -->

                                    </div>
                                    <!-- <div class="group-material">
                                            <input name="palabrasClaves" type="text"
                                                class="tooltips-general material-control"
                                                placeholder="Escribe aquí las Palabras Claves de la Tesis"
                                                data-toggle="tooltip" data-placement="top"
                                                title="Escribe las Palabras Claves de la Tesis">
                                           
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label>Palabras Claves</label>
                                        </div> -->
                                        <img src="<?php echo $informacionTesis['TapaTesis'] ?>"
                                            style="float:right;width:350px;height:250px;">                

                                    <div class="group-material">

                                        <input name="file" type="file" class="tooltips-general material-control">
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Imagen de la Tapa de la Tesis</label>
                                    </div>

                                    <div class="group-material">

                                        <input name="pdf" type="file" class="tooltips-general material-control">
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Documento Completo</label>
                                    </div>


                                    <button type="submit" class="btn btn-info btn-block">Actulalizar
                                        Datos</a></button>



                                    <?php
                                                    // break; 
                                                    
                                                        endforeach;
                                        ?>

                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </form>

        <!-- FINAL IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->

        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                    </div>
                    <div class="modal-body">
                        En caso de tener algun problema, comunicarse a: christopherarce2357@gmail.com
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Proyecto para la materia de Base de Datos II.
                            <br> Docente: Ing. Maria Hurtado
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Christopher Arce Diaz <i
                                    class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i
                                    class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2020 Christopher Arce Diaz</div>
        </footer>
    </div>

    <!-- <script src="../js/busqueda.js"></script> -->
</body>

</html>