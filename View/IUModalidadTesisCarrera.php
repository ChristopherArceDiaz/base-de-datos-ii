<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/Carrera.php");
        
        $objCarrera = new Carrera();

        $listaCantidadModalidadTeisCarrera = $objCarrera->listaCantidadModalidadTeisCarrera();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>ReporteCarrera</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />

    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>


</head>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button" style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i> 
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
                <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p>
            </div>
            <div class="full-reset nav-lateral-list-menu">
            <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                    <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                    <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de
                            Usuarios</a></li>
                    <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Lista de Tesis</a></li>

                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="IURegistrarTesis.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp;
                                    Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona.php"><i
                                        class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a>
                            </li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Reportes y estadísticas</a></li>

                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                   <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:
                    <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?></span>
                </li>
                <li  class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom" title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li  class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom" title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Sistema bibliotecario <small>Reporte por Carrera</small></h1>
              <!-- <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Puedes buscar una tesis por Autor, Título o Tipo de Tesis
                </div> -->
            </div>
        </div>
      
        <!-- <div class="container-fluid" style="margin: 0 0 50px 0;">
            <h2 class="text-center" style="margin: 0 0 25px 0;">Categorías</h2>
            <ul class="list-unstyled text-center list-catalog-container">
            <li class="list-catalog">Proyecto de Grado</li>
                <li class="list-catalog">Tesis</li>
                <li class="list-catalog">Trabajo Dirigido</li>
            </ul>
        </div> -->


<!-- IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->
<table class="table table-bordered table-sm" id="busquedaTitulo">
            <thead>
              <tr>
                <td>N°</td>
                <!-- <td>Facultad</td> -->
                <td>Carrera</td>
                <td>Cantidad por Modalidad</td>
                <td>Modalidad de Tesis</td>
                <!-- <td>Autor</td>
                <td>Titulo</td>
                <td>Tipo de Bibliografia</td> -->
                <!-- <td>Fecha</td> -->
                <!-- <td>Actuallizar</td>
                <td>Eliminar</td> -->
              </tr>
            </thead>
            <tbody>
      
      <?php
      echo '<pre>';
      $contut=1;
      foreach($listaCantidadModalidadTeisCarrera  as $cantidadModalidadTesis ):  ?>
        <tr>
          <th scope="row"><?php echo($contut);?></th>
          <!-- <td><?php echo $cantidadModalidadTesis['Facultad'] ?></td> -->
          <td><?php echo $cantidadModalidadTesis['Carrera'] ?></td>
          <td><?php echo $cantidadModalidadTesis['Cantidad'] ?></td>
          <td><?php echo $cantidadModalidadTesis['Modalidad'] ?></td>
          <!-- <td><?php echo $cantidadModalidadTesis['Autor'] ?></td>
          <td><a href="IUListaModalidadTesisInformacion.php"><?php echo $cantidadModalidadTesis['Titulo'] ?></a></td>
          <td><?php echo $cantidadModalidadTesis['Nombre'] ?></td> -->
          <!-- <td><?php echo $cantidadModalidadTesis['Fecha'] ?></td> -->
        
          <!-- <td>
              <a href="#"><i class="fas fa-edit"></i> Editar</a>
          </td>
          <td>
              <a href="#"><i class="fas fa-trash-alt"></i> Eliminar</a>
          </td>
       -->
        </tr>
        <?php 
          $contut++;
        endforeach; ?>
      </tbody>
           
          </table>



<!-- FINAL IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->

        <section class="full-reset text-center" style="padding: 40px 0;">
            <!-- <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-face"></i></div>
                <div class="tile-name all-tittles">administradores</div>
                <div class="tile-num full-reset">7</div>
            </article> -->
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-book"></i></div>
                <div class="tile-name all-tittles">Cantidad Modalidad de Tesis</div>
                <div class="tile-num full-reset">77</div>
            </article>
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-bookmark-outline"></i></div>
                <div class="tile-name all-tittles">categorías</div>
                <div class="tile-num full-reset">11</div>
            </article>
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-assignment-account"></i></div>
                <div class="tile-name all-tittles">secciones</div>
                <div class="tile-num full-reset">17</div>
            </article>
        </section>

        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                En caso de tener algun problema, comunicarse a: christopherarce2357@gmail.com
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Proyecto para la materia de Base de Datos II.
                            <br> Docente: Ing. Maria Hurtado
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Christopher Arce Diaz <i class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2020 Christopher Arce Diaz</div>
        </footer>
    </div>

    <script src="../js/busqueda.js"></script>
</body>
</html>