<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/DocumentoTesis.php");
        require_once("../Model/TipoTesis.php");
        require_once("../Model/Facultad.php");
        require_once("../Model/Carrera.php");
        
        $objDocumentoTesis = new DocumentoTesis();

        $listaDocumentoTesis = $objDocumentoTesis->listaDocumentoTesis();


        $listaAnioTesis = $objDocumentoTesis->listaAnioTesis();

        $objetoTipoTesis = new TipoTesis();
        $listaTipoTesis = $objetoTipoTesis->listaTipoTesis();


        $objetoFacultad = new Facultad();

        $existeDatosFacultad = $objetoFacultad->listaFacultad();

        $objetoCarrera = new Carrera();

        $listaCarrera = $objetoCarrera->listaCarrera();
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Administrador</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/texto.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>


</head>

<body>
<?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>

            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                         <li><a href="IUListaPersonalTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona2.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:
                    <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?>
                    </span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Catálogo de Tesis</small></h1>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Puedes buscar una tesis por Autor, Título o Tipo de Tesis
                </div>
            </div>
        </div>


        <!-- Filtro de busqueda -->



        <h2>Filtro de Busquedas</h2>
        <!-- <form class="form-inline"> -->
        <div class="form-group" style="width:800px; margin:0 auto;">
            <label for="busqueda">Busqueda General: </label>
            <input type="text" name="busqueda" id="busqueda" style="width:400px">
        </div>
        <br>
        <div class="form-inline">
            <div class="form-group">
                <label for="facultad">Facultad: </label>
                <select class="form-control" id="facultad" name="facultad" style="width:290px"
                    onchange="ShowFacultad();">
                    <option value="0">Seleciona facultad</option>
                    <?php
        foreach($existeDatosFacultad as $facultad){ ?>

                    <option value='<?php echo $facultad['idFacultad'];?>'><?php echo $facultad['nombre'];?></o>

                        <?php
         }
         ?>
                </select>
            </div>
            <div class="form-group">
                <label for="carrera">Carrera: </label>
                <select class="form-control" id="carrera" name="carrera" style="width:260px" onchange="ShowCarrera();">
                    <option value="0">Seleciona Carrera</option>
                    <?php
        foreach($listaCarrera as $carrera){ ?>

                    <option value='<?php echo $carrera['idFacultad'];?>'><?php echo $carrera['nombre'];?></option>

                    <?php
         }
         ?>
                </select>
            </div>
            <div class="form-group">
                <label for="tipoTesis">Tipo de Tesis: </label>
                <select class="form-control" id="tipoTesis" name="tipoTesis" style="width:130px"
                    onchange="ShowTipoTesis();">
                    <option value="0">Tipo de Tesis</option>
                    <?php foreach($listaTipoTesis as $tipoTesis){ ?>
                    <option value='<?php echo $tipoTesis['idTipoTesis'];?>'><?php echo $tipoTesis['nombre'];?></option>
                    <?php }?>
                </select>
            </div>
            <div class="form-group">
                <label for="tipoTesis">Año: </label>
                <select class="form-control" id="anio" name="anio" style="width:130px" onchange="ShowAnio();">
                    <option value="0">Año:</option>
                    <?php foreach($listaAnioTesis as $AnioTesis){ ?>
                    <option value='<?php echo $AnioTesis['Anio'];?>'><?php echo $AnioTesis['Anio'];?></option>
                    <?php }?>
                </select>
            </div>

        </div>

        <br>


        <!-- <button id="enviar" name="buscar" type="submit" class="btn btn-info dropdown-toggle">Buscar</button> -->

        <!-- </form> -->
        <!-- IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->
        <table class="table table-bordered table-sm" id="busquedaTitulo">
            <thead>
                <tr>
                    <td WIDTH="2">#</td>
                    <td WIDTH="100">Codigo</td>
                    <td WIDTH="200">Autor</td>
                    <td WIDTH="300">Titulo</td>
                    <td WIDTH="100">Tipo de Bibliografia</td>
                    <td WIDTH="10">Actualizar</td>
                    <td WIDTH="10">Eliminar</td>

                </tr>
            </thead>
            <tbody id="tBodyData">


            </tbody>

        </table>


        <?php
           include 'footer.php'
           ?>

        <!-- <script src="../js/index.js"></script> -->
        <!-- <script src="../js/busqueda.js"></script> -->
        <script type="text/javascript" src="../js/busquedaUsuarioExterno.js"></script>
        <script type="text/javascript" src="../js/filtroBusquedasAdmi.js"></script>

</body>

</html>