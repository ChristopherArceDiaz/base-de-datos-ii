<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/Persona.php");
        
        $objPersona = new Persona();

        $listaDePersonalTesis = $objPersona->listaDePersonalTesis();

        $listaDeRolPersonalTesis = $objPersona->listaDeRolPersonalTesis();

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Lista de Personas</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>


</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>

            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                         <li><a href="IUListaPersonalTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona2.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:
                        <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?></span>

                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Administración Usuarios</small></h1>
            </div>
        </div>

        <!-- <div class="container-fluid"> -->
        <h2 class="text-center all-tittles">Lista de Personas Registradas en el Sistema</h2>

        <!-- <div class="form-group" style="width:800px; margin:0 auto;">
            <label for="busqueda">Busqueda General: </label>
            <input type="text" name="busqueda" id="busqueda" style="width:400px">
        </div> -->
        <br>


        <!-- modal -->

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Agregar
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Agregar Personal de Tesis</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form autocomplete="off" method="post" action="../Controller/LNRegistrarPersonalTesis.php"
                enctype="multipart/form-data" class="needs-validation" novalidate>
                <div class="container-flat-form">
                    <div class="title-flat-form title-flat-blue">Nuevo Personal</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">



                            <div class="group-material">
                                <span>Rol</span>
                                <select name="idRol" id="idRol" class="tooltips-general material-control"
                                    data-toggle="tooltip" data-placement="top" title="Elige la Tipo de Tesis del libro">
                                    <option value="" disabled="" selected="">Selecciona un Tipo de Rol</option>
                                    <?php foreach($listaDeRolPersonalTesis as $rol){ ?>
                                    <option value='<?php echo $rol['idRolPersonalTesis'];?>'><?php echo $rol['nombre'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="group-material">
                                <input name="ci" id="ci" type="text" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu ci" data-toggle="tooltip" data-placement="top"
                                    title="Escribe tu numero de ci, solamente números" pattern="[0-9]{7,8}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>CI</label>
                            </div>
                            <div class="group-material">
                                <input name="primerNombre" id="primerNombre" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu primer Nombre" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Apellido Paterno, solamente letras"
                                    pattern="[A-Za-z]{3,20}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Primer Nombre</label>
                            </div>
                            <div class="group-material">
                                <input name="segundoNombre" id="segundoNombre" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Segundo Nombre, solamente letras"
                                    pattern="[A-Za-z]{3,20}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Segundo Nombre</label>
                            </div>
                            <div class="group-material">
                                <input name="primerApellido" id="primerApellido" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Apellido Paterno, solamente letras"
                                    pattern="[A-Za-z]{3,20}">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Apellido Paterno</label>
                            </div>
                            <div class="group-material">
                                <input name="segundoApellido" id="segundoApellido" type="text"
                                    class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu Apellido Materno, solamente letras"
                                    pattern="[A-Za-z]{3,20}">

                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Apellido Materno</label>
                            </div>
          
                            <div class="group-material">

                                <input name="file" id="file" type="file" class="tooltips-general material-control">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Fotografia</label>
                            </div>
                            

                            <p class="text-center">
                                <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i
                                        class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                <button id="enviar" type="submit" class="btn btn-primary"><i
                                        class="zmdi zmdi-floppy"></i>
                                    &nbsp;&nbsp; Guardar</button>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->
        <table class="table table-bordered table-sm" id="busquedaTitulo">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Personal</td>
                    <td>ci</td>
                    <td>Rol</td>
                    <td>Estado</td>
                    <td>Actualizar</td>
                    <td>Eliminar</td>

            </thead>
            <tbody>

                <?php
      echo '<pre>';
      $contut=1;
      foreach($listaDePersonalTesis  as $personal ):  ?>
                <tr>
                    <th scope="row"><?php echo($contut);?></th>
                    <td><?php echo $personal['Usuario'] ?></td>
                    <td><?php echo $personal['ci'] ?></td>
                    <td><?php echo $personal['Rol'] ?></td>
                    <td><?php echo $personal['estado'] ?></td>


                    <td><button class="btn btn-info" type='submit'><a
                                href="IUActualizarPersonalTesis.php?idPersonalTesis=<?php echo $personal['idPersonalTesis']; ?>"><i
                                    class="zmdi zmdi-refresh"></i></a></button></td>

                    <td><button class="btn btn-danger" type='submit'><a
                                href="../Controller/LNInactivoPersonalTesis.php?idPersonalTesis=<?php echo $personal['idPersonalTesis']; ?>"><i
                                    class="zmdi zmdi-refresh"></i></a></button>&nbsp;
                        <button class="btn btn-success" type='submit'><a
                                href="../Controller/LNActivoPersonalTesis.php?idPersonalTesis=<?php echo $personal['idPersonalTesis']; ?>"><i
                                    class="zmdi zmdi-refresh"></i></a></button>
                    </td>




                </tr>
                <?php 
          $contut++;
        endforeach; ?>  
            </tbody>

        </table>


        <?php
           include 'footer.php'
           ?>

        <script src="../js/busqueda.js"></script>
        <script src="../js/validaciones.js"></script>
        <!-- <script src="../js/busquedaListaPersonalTesis.js"></script> -->
</body>

</html>