<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/Facultad.php");
        
        $objFacultad = new Facultad();

        $listaCantidadTeisFacultad = $objFacultad->listaCantidadTeisFacultad();
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>ReporteFacultad</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>



    <!-- graficos -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>


    <!-- <script src="../js/reportesGraficos.js"></script> -->
    <!--Fin  graficos -->


    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>


</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>
                <!-- <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p> -->
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                         <li><a href="IUListaPersonalTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona2.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:
                    <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?>
                    </span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Reporte por Facultad</small></h1>

            </div>
        </div>
        <?php

        date_default_timezone_set('America/La_Paz');  
        $fechaActual = date('d-m-Y');
        ?>
        <h1>Fecha Actual del Reporte:<?php echo $fechaActual;  ?></h1>
        <?php
        
  
  
?>
        <div class="top-area">
            <div class="container">
                <div class="row">

                    <div class="col-sm-6 col-md-6">

                    </div>
                    <div class="col-sm-6 col-md-6">
                        <!-- <p class="bold text-right">Contacto +65315139</p> -->
                        <form action="IUBusquedaFechas.php" method="get">

                            <h3 class="bold text-left">Reporte entre 2 años</h3> <br>
                            <label for="">Fecha Inicio </label>
                            <input type="date" name="fecha1" id="fecha1">
                            <label for="">Fecha Fin </label>
                            <input type="date" name="fecha2" id="fecha2">
                            <input type="submit" value="Buscar" class="btn btn-info btn-lg">

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="container navigation">



            <!-- IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->
            <table class="table table-bordered table-sm" id="busquedaTitulo">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Facultad</td>
                        <td>Cantidad de Tesis</td>
                        <!-- <td>Sigla de la Facultad</td> -->
                        <td>Cantidad de Tesis por Carrera </td>
                        <td>Cantidad de Tesis por Año</td>

                </thead>
                <tbody>

                    <?php
      echo '<pre>';
      $contut=1;
      foreach($listaCantidadTeisFacultad  as $cantidadTesis ):  ?>
                    <tr>
                        <th scope="row"><?php echo($contut);?></th>
                        <td><?php echo $cantidadTesis['Nombre'] ?></td>
                        <td><?php echo $cantidadTesis['Cantidad'] ?></td>
                        <!-- <td><?php echo $cantidadTesis['Sigla'] ?></td> -->


                        <td><a href="IUReporteFacultadCarrera.php?FacultadCarrera=<?php echo $cantidadTesis['idFacultad']; ?>"
                                class="btn btn-dark">Tesis por Carrera</a></td>
                        <td><a href="IUReporteFacultadAnio.php?FacultadAnio=<?php echo $cantidadTesis['idFacultad']; ?>"
                                class="btn btn-dark">Tesis por año por carrera</a></td>




                    </tr>
                    <?php 
          $contut++;
        endforeach; ?>
                </tbody>

            </table>

            <div class="chart-container" style="height: 400px">
                <canvas id="grafico1" width="200" height="200"></canvas>
                <script>
                var ctx = document.getElementById('grafico1');

                var grafico1 = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: [
                            <?php  foreach ($listaCantidadTeisFacultad as $cantidadTesis) { ?> '<?php echo $cantidadTesis['Nombre']?>',
                            <?php } ?>
                        ],
                        datasets: [{
                            label: '# of Votes',
                            data: [
                                <?php  foreach ($listaCantidadTeisFacultad as $cantidadTesis) { ?>
                                <?php echo $cantidadTesis['Cantidad']?>,
                                <?php } ?>
                            ],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
                </script>
            </div>
        </div>
        <!-- Reporte Grafico -->





        <!-- Fin Reporte Gradico -->


        <!-- FINAL IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->
        <?php
           include 'footer.php'
           ?>


        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script> -->
        <script src="../js/busqueda.js"></script>


        <!-- <script src="../js/reportesGraficos.js"></script> -->

</body>

</html>