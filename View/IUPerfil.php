<?php
     require_once("../Model/Conexion.php");
     require_once("../Model/Persona.php");
     require_once("../Model/Rol.php");

     $objPersona = new Persona();
     $perfil = $objPersona->perfil($_REQUEST['idPerfil']);

     $objetoRol = new Rol();
     $listaRol = $objetoRol->listaRol();

    // echo $_REQUEST['idPerfil'];
    // exit;
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Administrador</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>

            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                         <li><a href="IUListaPersonalTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona2.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador: <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?></span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Datos Personales</small></h1>
            </div>
        </div>
     

        <div class="container">
            <br>
            <div class="row">
                <div class="cold-md-5">
                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentacion" class="active"><a href="#seccion1" aria-controls="seccion1"
                                    data-toggle="tab" role="tab">Datos Generales</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="seccion1">

                                <div class="card bg-info" style="width: 60rem;">
                                    <?php
                                    foreach($perfil as $informacionPerfil ): ?>

                                    <img src="<?php echo $informacionPerfil['foto'] ?>"
                                        style="float:right;width:350px;height:250px;">

                                    <?php
                              
                                            break; 
                                            
                                                endforeach;
                        
                                    foreach($perfil as $informacionPerfil ):  ?>

                                    <h3>
                                        <p>Persona: <?php echo $informacionPerfil['Persona'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Cedula de Identidad: <?php echo $informacionPerfil['ci'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Telefono: <?php echo $informacionPerfil['telefono'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Estado: <?php echo $informacionPerfil['estado'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Usuario: <?php echo $informacionPerfil['usuario'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Fecha de Registro: <?php echo $informacionPerfil['registro'] ?></p>
                                    </h3>
                                    <h3>    
                                        <p>Fecha de Actualizacion: <?php echo $informacionPerfil['actualizacion'] ?></p>
                                    </h3>


                                    <?php
                                                // break; 
                                                
                                                    endforeach;
                                        ?>
                                        <br>
                                        <!-- <a class="btn btn-info btn-block" href="IUActualizarPersona.php?idPersona=<?php echo $_SESSION['idPersona'] ?>" role="button" style="font-size: 25px;">Actualizar</a> -->
                                    <div class="boton">
                                        <button type="button" class="btn btn-info btn-block"><a
                                                href="IUActualizarPerfil.php?idPersona=<?php echo $_SESSION['idPersona'] ?>" style="font-size: 20px;">Actulalizar Datos</a></button>
                                        <style>
                               
                                        .boton {
                                            width: 100%;
                                            display: flex;
                                            align-items: center;
                                            justify-content: center;
                                            font-size: 10px;
                                        }
                                        </style>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>
        <?php
           include 'footer.php'
           ?>
       
        <script src="../js/modalActualizarPerfil.js"></script>
</body>

</html>