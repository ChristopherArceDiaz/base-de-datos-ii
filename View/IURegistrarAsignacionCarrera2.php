<?php
         
        require_once("../Model/Conexion.php");
        require_once("../Model/Facultad.php");
        require_once("../Model/Carrera.php");
        require_once("../Model/TipoTesis.php");
        require_once("../Model/AsignacionCarrera.php");
        require_once("../Model/Persona.php");
        
        $objPersona = new Persona();
        $listaDePersonasRegistro = $objPersona->listaDePersonasRegistro();
        
        $objetoFacultad = new Facultad();
        $listaFacultad = $objetoFacultad->listaFacultad();

        $objetoCarrera = new Carrera();
        // $listaCarreraRegistro = $objetoCarrera->listaCarreraRegistro($_REQUEST['infor']);
        $listaCarrera = $objetoCarrera->listaCarrera();


        $objetoTipoTesis = new TipoTesis();
        $listaTipoTesis = $objetoTipoTesis->listaTipoTesis();

       


        // $objetoAsignacionCarrera = $_GET['idUltimo'];
        // echo $objetoAsignacionCarrera;
        // echo $_GET['idUltimo'];

        date_default_timezone_set('America/La_Paz');  
        $fechaActual = date('Y-m-d H:i:s');


?>


<!DOCTYPE html>
<html lang="es">

<head>
    <title>Registrar Tesis</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }


    //   echo $_SESSION['idUltimo'];
    //   exit;
    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>

            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp;Tesis</a></li>
                         <li><a href="IUListaPersonalTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Personal de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarTesis.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nueva Tesis</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:
                    <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?>
                    </span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Añadir Tesis</small></h1>
            </div>
        </div>
        <!-- <div class="container-fluid" style="margin: 50px 0;">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <img src="../assets/img/flat-book.png" alt="pdf" class="img-responsive center-box"
                        style="max-width: 110px;">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Bienvenido a la sección para agregar nuevas tesis, deberas de llenar todos los
                    campos para poder registrar el libro
                </div>
            </div>
        </div> -->
        <div class="container-fluid">

            <form autocomplete="off" method="post" action="../Controller/LNRegistrarAsignacionCarrer2.php">
                <div class="container-flat-form">
                    <div class="title-flat-form title-flat-blue">Asignación de Carrera a la Nueva Tesis</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">

                        <div class="group-material">
                                <input name="usuario" type="hidden"
                                    class="tooltips-general material-control" value="<?php echo $_GET['usuario'];  ?>">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                              
                            </div>


                            <div class="group-material">
                                <input name="contrasenia" type="hidden"
                                    class="tooltips-general material-control" value="<?php echo $_GET['contrasenia'];  ?>">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                              
                            </div>

                            <!-- TABLA ASIGNACIONCARRERA -->

                            <div class="group-material">
                                <span>Carrera</span>
                                <select name="idCarrera" class="tooltips-general material-control" data-toggle="tooltip"
                                    data-placement="top" title="Elige la carrera de la Tesis">
                                    <option value="" disabled="" selected="">Selecciona una Carrera</option>
                                    <?php foreach($listaCarrera as $carrera){ ?>
                                    <option value='<?php echo $carrera['idCarrera'];?>'><?php echo $carrera['nombre'];?>
                                    </option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="group-material">
                                <input name="idPersona" type="hidden"
                                    class="tooltips-general material-control" value="<?php echo $_GET['idUltimo'];  ?>">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                              
                            </div>
                            

                            <p class="text-center">
                                <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i
                                        class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                <button type="submit" class="btn btn-primary"><i class="zmdi zmdi-floppy"></i>
                                    &nbsp;&nbsp; Guardar</button>
                            </p>
                        </div>
                    </div>
                </div>
            </form>
            
        </div>


        <?php
           include 'footer.php'
           ?>

</body>

</html>