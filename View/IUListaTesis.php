<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/DocumentoTesis.php");
        require_once("../Model/TipoTesis.php");
        require_once("../Model/Facultad.php");
        require_once("../Model/Carrera.php");

   
        
        $objDocumentoTesis = new DocumentoTesis();

        $listaDocumentoTesis = $objDocumentoTesis->listaDocumentoTesis();

        // $listaTesis = $objDocumentoTesis->listaTesis();
        
        $objetoTipoTesis = new TipoTesis();
        $listaTipoTesis = $objetoTipoTesis->listaTipoTesis();


        $objetoFacultad = new Facultad();

        $existeDatosFacultad = $objetoFacultad->listaFacultad();


        $objetoCarrera = new Carrera();

        $listaCarrera = $objetoCarrera->listaCarrera();
        // exit;
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Catálogo</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <scrip src="../js/sweet-alert.min.js"></scrip>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/texto.css">
    <link rel="stylesheet" type="text/css" href="../js/select/css/select2.css">
    <scrip src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></scrip>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    

</head>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button" style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i> 
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
                <!-- <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p> -->
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="../index.html"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>   
                    
                    <li><a href="View/IUListaTesis.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Tesis</a></li>   
                </ul>
                   
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                   <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a al Sistema Bibliotecario</span>
                </li>
                <li  class="tooltips-general exit-system-button" data-href="index.html" data-placement="bottom" title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li  class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom" title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
                <li>
                    <a href="../Login.php" class="btn btn-success my-2 my-sm-0">Login</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Sistema bibliotecario <small>Catálogo de Tesis</small></h1>
              <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Puedes buscar una tesis por Autor, Título o Tipo de Tesis
                </div>
            </div>
        </div>
 





            <h2>Filtro de Busquedas</h2>

            <!-- Filtro de busqueda -->
            <form class="form-inline"> <!-- action="IUBusquedaUsuariosLogueados.php" method="get"-->
   
   <div class="form-group">
       <label for="facultad">Facultad:   </label>
       <select class="form-control" id="facultad" name="facultad" style="width:290px" onchange="ShowFacultad();">
       <option value="0">Seleciona facultad</option>  
       <?php
        foreach($existeDatosFacultad as $facultad){ ?>
                             
           <option value='<?php echo $facultad['idFacultad'];?>'><?php echo $facultad['nombre'];?></o>
           
          <?php
         }
         ?>
       </select>
   </div>
   <div class="form-group">
       <label for="carrera">Carrera:   </label>
       <select class="form-control" id="carrera" name="carrera" style="width:260px" onchange="ShowCarrera();">
       <option value="0">Seleciona Carrera</option>  
       <?php
        foreach($listaCarrera as $carrera){ ?>
                                   
           <option value='<?php echo $carrera['idFacultad'];?>'><?php echo $carrera['nombre'];?></option>
           
          <?php
         }
         ?>
       </select>
   </div>
   <div class="form-group">
   <label for="tipoTesis">Tipo de Tesis:   </label>
       <select class="form-control" id="tipoTesis" name="tipoTesis" style="width:130px" onchange="ShowTipoTesis();">
           <option value="0">Tipo de Tesis</option>
               <?php foreach($listaTipoTesis as $tipoTesis){ ?>
                   <option value='<?php echo $tipoTesis['idTipoTesis'];?>'><?php echo $tipoTesis['nombre'];?></option>
               <?php }?>
       </select>
   </div>

   
   
   <!-- <button id="enviar" name="buscar" type="submit" class="btn btn-info dropdown-toggle">Buscar</button> -->

</form>


<!-- IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->
<table class="table table-bordered table-sm" id="busquedaTitulo">
            <thead>
              <tr>
                <td>#</td>
                <td WIDTH="150">Codigo</td>
                <td WIDTH="200">Autor</td>
                <td WIDTH="650">Titulo</td>
                <td WIDTH="150">Tipo de Bibliografia</td>

              </tr>
            </thead>
            <tbody id="tBodyData">
      

      </tbody>
           
          </table>





          <?php
           include 'footer.php'
           ?>   

  

    <script src="../js/filtroBusquedaUsuarioExterno.js"></script>
    <!-- <script type="text/javascript" src="../js/filtroBusquedas.js"></script> -->



</body>
</html>