IUListaTesisInformacionEstudiante.php

<?php
    require_once("../Model/Conexion.php");
    require_once("../Model/DocumentoTesis.php");
    // require_once("../Model/DocumentoTesis.php");
    // require_once("../Controller/LNListaTesis.php");

    $objetoDocumentoTesis = new DocumentoTesis();
    // $objetoDocumentoTesis = new LNListaTesis();

    $existeDatosTesis = $objetoDocumentoTesis->informacionDocumentoTesisTitulo($_REQUEST['infor']);


?>

<!DOCTYPE html>
<html lang="es">

<head>
    <title>Administrador</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <!-- <link rel="stylesheet" href="../css/texto.css"> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>



    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.2.4/pdfobject.min.js"></script>

</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>
                <!-- <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p> -->
            </div>
            <div class="full-reset nav-lateral-list-menu">
            <ul class="list-unstyled">
                    <li><a href="IUPrincipalEstudiante.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Inicio</a></li>
                            <li><a href="IUPerfilEstudiante.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                    <li><a href="IUListaTesisEstudiante.php"><i
                                class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Tesis</a></li>


                    <!-- <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li> -->
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador:
                        <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?>
                    </span>
                </li>
                <li class="tooltips-general exit-system-button"
                    data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Informacion de la Tesis</small></h1>

            </div>
        </div>



        <div class="container">
            <br>
            <div class="row">
                <div class="cold-md-5">
                <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentacion" class="active"><a href="#seccion1" aria-controls="seccion1"
                                    data-toggle="tab" role="tab">Datos Generales</a></li>
                            <li role="presentacion"><a href="#seccion5" aria-controls="seccion5" data-toggle="tab"
                                    role="tab">Fotografia Autor (es)</a></li>
                            <li role="presentacion"><a href="#seccion6" aria-controls="seccion6" data-toggle="tab"
                                    role="tab">Fotografia Tutor/Revisor</a></li>
                            <li role="presentacion"><a href="#seccion2" aria-controls="seccion2" data-toggle="tab"
                                    role="tab">Resumen</a></li>
                            <li role="presentacion"><a href="#seccion3" aria-controls="seccion3" data-toggle="tab"
                                    role="tab">Introduccion</a></li>
                            <li role="presentacion"><a href="#seccion4" aria-controls="seccion4" data-toggle="tab"
                                    role="tab">Documento Completo</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="seccion1">

                                <div class="card bg-info" style="width: 60rem;">
                                    <?php
                                    foreach($existeDatosTesis as $informacionTesis ): ?>

                                    <img src="<?php echo $informacionTesis['TapaTesis'] ?>"
                                        style="float:right;width:400px;height:300px;">

                                    <?php

                                            break;

                                                endforeach;

                                    foreach($existeDatosTesis as $informacionTesis ):  ?>

                                    <h3>
                                        <p>Autor: <?php echo $informacionTesis['Autor'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Titulo: <?php echo $informacionTesis['Titulo'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Año: <?php echo $informacionTesis['Anio'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Tipo de Bibliografía: <?php echo $informacionTesis['TipoBibliografia'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Facultad: <?php echo $informacionTesis['Facultad'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Carrera: <?php echo $informacionTesis['Carrera'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Codigo: <?php echo $informacionTesis['Codigo'] ?></p>
                                    </h3>

                                    <?php
                                                // break;

                                                    endforeach;
                                        ?>

                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="seccion2">
                                <h2>Resumen</h2><br>
                                <?php
                                        foreach($existeDatosTesis as $informacionTesis ):  ?>

                                <p class="resumen"><?php echo $informacionTesis['Resumen'] ?></p>

                                <?php
                                                    break;

                                                        endforeach;
                                        ?>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="seccion3">
                                <h2>Introduccion</h2><br>
                                <?php
                                    foreach($existeDatosTesis as $informacionTesis ):  ?>
                                <p class="resumen"><?php echo $informacionTesis['Introduccion'] ?></p>
                                <?php
                                                break;

                                                    endforeach;
                                    ?>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="seccion4">
                                <!-- <iframe src="../TesisPDF/HUAMAN VARAS JOSELYN  - HUAYANCA QUISPE CARLOS.pdf" height="500" width="500"></iframe> -->

                                <!-- <h2>Documento Completo</h2><br> -->
                                <!-- <iframe src="../TesisPDF/HUAMAN VARAS JOSELYN  - HUAYANCA QUISPE CARLOS.pdf" height="500" width="500" title="Iframe Example"></iframe> -->

                                <?php
                                        foreach($existeDatosTesis as $informacionTesis ):  ?>

                                <iframe type="application/pdf" src="<?php echo $informacionTesis['DocumentoCompleto']; ?>"
                                            height="650" width="1000" title="Iframe Example"></iframe>

                                    <!-- <iframe src="../TesisPDF/null.pdf" type="application/pdf" height="650" width="1000"
                                        title="Iframe Example"></iframe> -->

                                <!-- <embed src="<?php echo $informacionTesis['DocumentoCompleto']; ?>" width="500" height="375" type="application/pdf"> -->

                                <!-- <object data="<?php echo $informacionTesis['DocumentoCompleto']; ?>" width="500" height="200"></object> -->


                                <!-- <script>     var viewer = $("#seccion4");PDFObject.embed("<?php echo $informacionTesis['DocumentoCompleto'] ?>", viewer);</script> -->
                                <?php
                                                    break;

                                                        endforeach;
                                        ?>

                            </div>

                            <div role="tabpanel" class="tab-pane" id="seccion5">
                                <div class="card bg-info" style="width: 60rem;">
                                    <?php

                                foreach($existeDatosTesis as $informacionTesis ):  ?>
                                    <h2 class="card-text">Autor (es)</h2>
                                    <img src="<?php echo $informacionTesis['fotografia'] ?>"
                                        style="float:right;width:400px;height:300px;">

                                   
                                        <img src="<?php echo $informacionTesis['foto'] ?>"
                                            style="float:left;width:300px;height:300px;">
                                   

                                    <?php
                                            break;

                                                endforeach;
                                ?>
                                </div>
                            </div>


                            <div role="tabpanel" class="tab-pane" id="seccion6">
                                <div class="card bg-info" style="width: 60rem;">
                                    <?php

                                foreach($existeDatosTesis as $informacionTesis ):  ?>
                                    <h2 class="card-text">Tutor / Revisores</h2>
                                    <img src="<?php echo $informacionTesis['fotografia'] ?>"
                                        style="float:right;width:400px;height:300px;">

                                    
                                        <img src="<?php echo $informacionTesis['foto'] ?>"
                                            style="float:left;width:300px;height:300px;">
                                   

                                    <?php
                                            break;

                                                endforeach;
                                ?>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>




        <!-- FINAL IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->

        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                    </div>
                    <div class="modal-body">
                        En caso de tener algun problema, comunicarse a: christopherarce2357@gmail.com
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Proyecto para la materia de Base de Datos II.
                            <br> Docente: Ing. Maria Hurtado
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Christopher Arce Diaz <i
                                    class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i
                                    class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2020 Christopher Arce Diaz</div>
        </footer>
    </div>

    <!-- <script src="../js/busqueda.js"></script> -->


</body>

</html>