<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/DocumentoTesis.php");
        // require_once("../Model/TipoTesis.php");
        // require_once("../Model/Facultad.php");
    

   
        
        $objDocumentoTesis = new DocumentoTesis();

        $busquedaUsuarioFCT = $objDocumentoTesis->busquedaUsuarioFCT($_REQUEST['facultad'],$_REQUEST['carrera'],$_REQUEST['idTipoTesis'],$_REQUEST['fecha']);

        $busquedaUsuarioFacultadCarrera = $objDocumentoTesis->busquedaUsuarioFacultadCarrera($_REQUEST['facultad'],$_REQUEST['carrera'],$_REQUEST['fecha']);

        $busquedaUsuarioAnio = $objDocumentoTesis->busquedaUsuarioAnio($_REQUEST['fecha']);

        $busquedaUsuarioExternoFCT = $objDocumentoTesis->busquedaUsuarioExternoFCT($_REQUEST['facultad'],$_REQUEST['carrera'],$_REQUEST['idTipoTesis']);

        $busquedaUsuarioExternoFacultadCarrera = $objDocumentoTesis->busquedaUsuarioExternoFacultadCarrera($_REQUEST['facultad'],$_REQUEST['carrera']);

        $busquedaUsuarioExternoFacultad = $objDocumentoTesis->busquedaUsuarioExternoFacultad($_REQUEST['facultad']);

        $busquedaUsuarioExternoTesis = $objDocumentoTesis->busquedaUsuarioExternoTesis($_REQUEST['idTipoTesis']);


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Catálogo</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/EstiloTexto.css">
    <link rel="stylesheet" type="text/css" href="../js/select/css/select2.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
    <script src="../js/select/js/select2.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    

</head>
<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button" style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i> 
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box" style="width:55%;">
                </figure>
                <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p>
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="../index.html"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-case zmdi-hc-fw"></i>&nbsp;&nbsp; Administración <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="institution.html"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Datos institución</a></li>
                           
                        </ul>
                    </li>
                  
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i>&nbsp;&nbsp; Catálogo de Tesis <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <!-- <li><a href="book.html"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo libro</a></li> -->
                            <li><a href="IUListaTesis.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Catálogo</a></li>
                        </ul>
                    </li>
                   
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                   <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Usuario Universal</span>
                </li>
                <li  class="tooltips-general exit-system-button" data-href="index.html" data-placement="bottom" title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li  class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom" title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li  class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
                <li>
                    <a href="../Login.php" class="btn btn-success my-2 my-sm-0">Login</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Sistema bibliotecario <small>Catálogo de libros</small></h1>
              <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Puedes buscar una tesis por Autor, Título o Tipo de Tesis
                </div>
            </div>
        </div>
 

        <!-- Filtro de busqueda -->
        <h2>Filtro de Busquedas</h2>

   <div class="form-inline">
            <div class="form-group">
                <label for="facultad">Facultad:   </label>
                <select class="form-control" id="facultad" name="facultad" style="width:260px">
               
                </select>
            </div>
            <div class="form-group">
                <label for="carrera">Carrera:   </label>
                <select class="form-control" id="carrera" name="carrera" style="width:260px">

                </select>
            </div>
            <div class="form-group">
            <label for="carrera">Tipo de Tesis:   </label>
                <select class="form-control" style="width:260px" name="idTipoTesis">
                
                </select>
            </div>
            <div class="form-group">
            <label for="carrera">Año:   </label>
                <input type="text" name="fecha" id="fecha">
            </div>
            <a href="IUListaTesisAdmi.php"><button id="enviar" name="buscar" type="submit" class="btn btn-info dropdown-toggle">Refrescar</button></a>
            </div>
 


            <?php

            if($_REQUEST['facultad'] && $_REQUEST['carrera'] && $_REQUEST['idTipoTesis'])
                {
                


                    ?>
                        <table class="table table-bordered table-sm" id="busquedaTitulo">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Codigo</td>
                        <td>Autor</td>
                        <td>Titulo</td>
                        <td>Tipo de Bibliografia</td>
                        <td>Actualizar</td>
                        <td>Eliminar</td>

                    </tr>
                    </thead>
                    <tbody>
            
            <?php
            echo '<pre>';
            $contut=1;
            foreach($busquedaUsuarioExternoFCT  as $busqueda2 ):  ?>
                <tr>
                <th scope="row"><?php echo($contut);?></th>
                
                <td><?php echo $busqueda2['Codigo'] ?></td>
                <td><a href="IUListaTesisInformacion.php?infor=<?php echo $busqueda2['idDocumentoTesis'] ?>"><?php echo $busqueda2['Autor'] ?></a></td>
                <td><?php echo $busqueda2['Titulo'] ?></td>
                <td><?php echo $busqueda2['Nombre'] ?></td>
                <td><a href="IUActualizarTesis.php?infor=<?php echo $busqueda2['idDocumentoTesis'] ?>">Actualizar</a></td>
                <td><a href="IUEliminaarTesis.php?eliminar=<?php echo $busqueda2['idDocumentoTesis'] ?>">Eliminar</a></td>
                </tr>
                <?php 
                $contut++;
                endforeach; ?>
            </tbody>
                
                </table>
                    <?php
                }
                else if ($_REQUEST['facultad']&& $_REQUEST['carrera']) 
                {
                
                    ?>
                    <table class="table table-bordered table-sm" id="busquedaTitulo">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Codigo</td>
                    <td>Autor</td>
                    <td>Titulo</td>
                    <td>Tipo de Bibliografia</td>
                    <td>Actualizar</td>
                        <td>Eliminar</td>

                </tr>
                </thead>
                <tbody>

            <?php
            echo '<pre>';
            $contut=1;
            foreach($busquedaUsuarioExternoFacultadCarrera  as $busqueda ):  ?>
            <tr>
            <th scope="row"><?php echo($contut);?></th>
            
            <td><?php echo $busqueda['Codigo'] ?></td>
            <td><a href="IUListaTesisInformacion.php?infor=<?php echo $busqueda['idDocumentoTesis'] ?>"><?php echo $busqueda['Autor'] ?></a></td>
            <td><?php echo $busqueda['Titulo'] ?></td>
            <td><?php echo $busqueda['Nombre'] ?></td>
            <td><a href="IUActualizarTesis.php?infor=<?php echo $busqueda['idDocumentoTesis'] ?>">Actualizar</a></td>
            <td><a href="IUEliminaarTesis.php?eliminar=<?php echo $busqueda['idDocumentoTesis'] ?>">Eliminar</a></td>
            </tr>
            <?php 
            $contut++;
            endforeach; ?>
            </tbody>
            
            </table>
                <?php
                }


                else if ($_REQUEST['facultad'] && $_REQUEST['carrera'] && $_REQUEST['idTipoTesis'] && $_REQUEST['fecha']) 
                {
                
                    ?>
                    <table class="table table-bordered table-sm" id="busquedaTitulo">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Codigo</td>
                    <td>Autor</td>
                    <td>Titulo</td>
                    <td>Tipo de Bibliografia</td>
                    <td>Actualizar</td>
                        <td>Eliminar</td>

                </tr>
                </thead>
                <tbody>

            <?php
            echo '<pre>';
            $contut=1;
            foreach($busquedaUsuarioFCT  as $busqueda ):  ?>
            <tr>
            <th scope="row"><?php echo($contut);?></th>
            
            <td><?php echo $busqueda['Codigo'] ?></td>
            <td><a href="IUListaTesisInformacion.php?infor=<?php echo $busqueda['idDocumentoTesis'] ?>"><?php echo $busqueda['Autor'] ?></a></td>
            <td><?php echo $busqueda['Titulo'] ?></td>
            <td><?php echo $busqueda['Nombre'] ?></td>
            <td><a href="IUActualizarTesis.php?infor=<?php echo $busqueda['idDocumentoTesis'] ?>">Actualizar</a></td>
            <td><a href="IUEliminaarTesis.php?eliminar=<?php echo $busqueda['idDocumentoTesis'] ?>">Eliminar</a></td>
            </tr>
            <?php 
            $contut++;
            endforeach; ?>
            </tbody>
            
            </table>
                <?php
                }



                else if ($_REQUEST['facultad']&& $_REQUEST['carrera'] && $_REQUEST['fecha']) 
                {
                
                    ?>
                    <table class="table table-bordered table-sm" id="busquedaTitulo">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Codigo</td>
                    <td>Autor</td>
                    <td>Titulo</td>
                    <td>Tipo de Bibliografia</td>
                    <td>Actualizar</td>
                        <td>Eliminar</td>

                </tr>
                </thead>
                <tbody>

            <?php
            echo '<pre>';
            $contut=1;
            foreach($busquedaUsuarioFacultadCarrera  as $busqueda ):  ?>
            <tr>
            <th scope="row"><?php echo($contut);?></th>
            
            <td><?php echo $busqueda['Codigo'] ?></td>
            <td><a href="IUListaTesisInformacion.php?infor=<?php echo $busqueda['idDocumentoTesis'] ?>"><?php echo $busqueda['Autor'] ?></a></td>
            <td><?php echo $busqueda['Titulo'] ?></td>
            <td><?php echo $busqueda['Nombre'] ?></td>
            <td><a href="IUActualizarTesis.php?infor=<?php echo $busqueda['idDocumentoTesis'] ?>">Actualizar</a></td>
            <td><a href="IUEliminaarTesis.php?eliminar=<?php echo $busqueda['idDocumentoTesis'] ?>">Eliminar</a></td>
            </tr>
            <?php 
            $contut++;
            endforeach; ?>
            </tbody>
            
            </table>
                <?php
                }


                else if ($_REQUEST['facultad']) 
                {

                    ?>
                    <table class="table table-bordered table-sm" id="busquedaTitulo">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Codigo</td>
                    <td>Autor</td>
                    <td>Titulo</td>
                    <td>Tipo de Bibliografia</td>
                    <td>Actualizar</td>
                        <td>Eliminar</td>

                </tr>
                </thead>
                <tbody>

            <?php
            echo '<pre>';
            $contut=1;
            foreach($busquedaUsuarioExternoFacultad  as $busqueda3 ):  ?>
            <tr>
            <th scope="row"><?php echo($contut);?></th>
            
            <td><?php echo $busqueda3['Codigo'] ?></td>
            <td><a href="IUListaTesisInformacion.php?infor=<?php echo $busqueda3['idDocumentoTesis'] ?>"><?php echo $busqueda3['Autor'] ?></a></td>
            <td><?php echo $busqueda3['Titulo'] ?></td>
            <td><?php echo $busqueda3['Nombre'] ?></td>
            <td><a href="IUActualizarTesis.php?infor=<?php echo $busqueda3['idDocumentoTesis'] ?>">Actualizar</a></td>
            <td><a href="IUEliminaarTesis.php?eliminar=<?php echo $busqueda3['idDocumentoTesis'] ?>">Eliminar</a></td>
            
            </tr>
            <?php 
            $contut++;
            endforeach; ?>
            </tbody>
            
            </table>
                <?php
                }
               
                else if ($_REQUEST['idTipoTesis']) 
                {
                    

                    ?>
                    <table class="table table-bordered table-sm" id="busquedaTitulo">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Codigo</td>
                    <td>Autor</td>
                    <td>Titulo</td>
                    <td>Tipo de Bibliografia</td>
                    <td>Actualizar</td>
                        <td>Eliminar</td>

                </tr>
                </thead>
                <tbody>

            <?php
            echo '<pre>';
            $contut=1;
            foreach($busquedaUsuarioExternoTesis  as $busqueda4 ):  ?>
            <tr>
            <th scope="row"><?php echo($contut);?></th>
            
            <td><?php echo $busqueda4['Codigo'] ?></td>
            <td><a href="IUListaTesisInformacion.php?infor=<?php echo $busqueda4['idDocumentoTesis'] ?>"><?php echo $busqueda4['Autor'] ?></a></td>
            <td><?php echo $busqueda4['Titulo'] ?></td>
            <td><?php echo $busqueda4['Nombre'] ?></td>
            <td><a href="IUActualizarTesis.php?infor=<?php echo $busqueda4['idDocumentoTesis'] ?>">Actualizar</a></td>
            <td><a href="IUEliminaarTesis.php?eliminar=<?php echo $busqueda4['idDocumentoTesis'] ?>">Eliminar</a></td>
            
            </tr>
            <?php 
            $contut++;
            endforeach; ?>
            </tbody>
            
            </table>
                <?php
                }


            ?>









        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                En caso de tener algun problema, comunicarse a: christopherarce2357@gmail.com
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Proyecto para la materia de Base de Datos II.
                            <br> Docente: Ing. Maria Hurtado
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Christopher Arce Diaz <i class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2020 Christopher Arce Diaz</div>
        </footer>
    </div>
      

    <!-- <script src="../js/index.js"></script> -->
    <script src="../js/busqueda.js"></script>
    <script type="text/javascript" src="../js/busquedaUsuarioExterno.js"></script>
    <!-- <script type="text/javascript" src="../js/funcionesMenu.js"></script> -->
</body>
</html>