<?php
    require_once("../Model/Conexion.php");
    require_once("../Model/Persona.php");
    require_once("../Model/Rol.php");
        
    $objPersona = new Persona();


    $existeDatosPersona = $objPersona->informacionPersona($_REQUEST['idPersona']);

    $objetoRol = new Rol();
    $listaRol = $objetoRol->listaRol();
    
    date_default_timezone_set('America/La_Paz');  
    $fechaActual = date('Y-m-d H:i:s');
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Catálogo</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <!-- <link rel="stylesheet" href="../css/EstiloTexto.css"> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>


</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

    //   if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
    //   {
    //     // header("location:../Controller/CerrarSesion.php");
    //     // header("location:Logout.php/");
     
    //     require_once("../Controller/CerrarSesion.php");
 
    //   }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>
                <!-- <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p> -->
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="IUPrincipalDocente.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp;
                            Inicio</a></li>
                    <li><a href="IUPerfilDocente.php?idPerfil=<?php echo $_SESSION['idPersona'];?>"><i
                                class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                    <li><a href="IUListaTesisDocente.php"><i
                                class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Tesis</a></li>


                    <!-- <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li> -->
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Docente: <?php 
                      echo $_SESSION['primerNombre']." ".$_SESSION['primerApellido'];
                        
                        ?></span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <!-- <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Sistema bibliotecario <small>Actualizar Usuario</small></h1>
         
            </div>
        </div> -->

        <form autocomplete="off" method="post" action="../Controller/LNActualizarPerfilDocente.php"
            enctype="multipart/form-data">
            <div class="container-flat-form">
                <div class="title-flat-form title-flat-blue">Actualizar Datos de Perfil</div>
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2">




                        <?php
              
                                foreach($existeDatosPersona as $informacionPersona ):  ?>
                        <!-- TABLA ASIGNACIONCARRERA -->
                        <input type="hidden" name="idPersona" id="idPersona"
                            value="<?php echo $informacionPersona['idPersona'] ?>">
                        <div class="group-material">
                            <label>Rol</label><br><br>
                            <select name="idRol" id="idRol" class="tooltips-general material-control" data-toggle="tooltip"
                                data-placement="top" title="Elige la Tipo de Tesis del libro">
                                <option value="" disabled="" selected=""><?php echo $informacionPersona['nombre'] ?>
                                </option>
                                <!-- <?php foreach($listaRol as $rol){ ?>
                                    <option value='<?php echo $rol['idRol'];?>'><?php echo $rol['nombre'];?></option>
                                      <?php }?> -->
                            </select>
                        </div>
                        <div class="group-material">
                            <input name="ci" id="ci" type="text" class="tooltips-general material-control"
                                placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                data-placement="top" title="Escribe el código de la Tesis, solamente números"
                                value="<?php echo $informacionPersona['ci'] ?>">
                            <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>CI</label>
                        </div>
                        <div class="group-material">
                            <input name="primerNombre" id="primerNombre" type="text" class="tooltips-general material-control"
                                placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                data-placement="top" title="Escribe el código de la Tesis, solamente números"
                                value="<?php echo $informacionPersona['primerNombre'] ?>">
                            <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>primerNombre</label>
                        </div>
                        <div class="group-material">
                            <input name="segundoNombre" id="segundoNombre" type="text" class="tooltips-general material-control"
                                placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                data-placement="top" title="Escribe el código de la Tesis, solamente números"
                                value="<?php echo $informacionPersona['segundoNombre'] ?>">
                            <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>segundoNombre</label>
                        </div>
                        <div class="group-material">
                            <input name="primerApellido" id="primerApellido" type="text" class="tooltips-general material-control"
                                placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                data-placement="top" title="Escribe el código de la Tesis, solamente números"
                                value="<?php echo $informacionPersona['primerApellido'] ?>">
                            <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>primerApellido</label>
                        </div>
                        <div class="group-material">
                            <input name="segundoApellido" id="segundoApellido" type="text" class="tooltips-general material-control"
                                placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                data-placement="top" title="Escribe el código de la Tesis, solamente números"
                                value="<?php echo $informacionPersona['segundoApellido'] ?>">
                            <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>segundoApellido</label>
                        </div>
                        <div class="group-material">
                            <input name="telefono" id="telefono" type="text" class="tooltips-general material-control"
                                placeholder="Escribe aquí tu telefono" data-toggle="tooltip" data-placement="top"
                                title="Escribe el Resumen de la Tesis"
                                value="<?php echo $informacionPersona['telefono'] ?>">
                            <!-- <input name="resumen" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el Resumen de la Tesis" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Escribe el Resumen de la Tesis"> -->
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Telefono</label>
                        </div>
                        <img src="<?php echo $informacionPersona['fotografia'] ?>"
                            style="float:right;width:350px;height:250px;">
                        <div class="group-material">



                            <!-- <input name="tapaTesis" type="file" class="tooltips-general material-control" placeholder="Inserta la Imagen de la Tapa de la Tesis" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Inserta la Imagen de la Tapa de la Tesis"> -->
                            <input name="file" type="file" class="tooltips-general material-control">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Fotografia</label>
                        </div>

                        <div class="group-material">
                            <input name="password" id="password" type="text" class="tooltips-general material-control"
                                placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                data-placement="top" title="Escribe el código de la Tesis, solamente números"
                                value="<?php echo $informacionPersona['contrasenia'] ?>">
                            <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Contraseña</label>
                            
                            <ul>
                                
                                <li id="mayus">Mayusculas</li>
                                <li id="special">Caracter especial</li>
                                <li id="numbers">Numeros</li>
                                <li id="lower">Minusculas</li>
                                <li id="len">Minimo 8 caracteres</li>
                            </ul>
                        </div>

                        <div class="group-material">

                            <input name="fechaActualizacion" type="hidden" class="tooltips-general material-control"
                                value="<?= $fechaActual?>">
                            <span class="highlight"></span>
                            <span class="bar"></span>

                        </div>


                        <p class="text-center">
                            <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i
                                    class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                            <button type="submit" class="btn btn-primary"><i class="zmdi zmdi-floppy"></i> &nbsp;&nbsp;
                                Actualizar</button>
                        </p>
                    </div>
                </div>
            </div>
        </form>
        <?php
                                            // break; 
                                            
                                                endforeach;
                                ?>
        <!-- FINAL IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->

        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                    </div>
                    <div class="modal-body">
                        En caso de tener algun problema, comunicarse a: christopherarce2357@gmail.com
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Proyecto para la materia de Base de Datos II.
                            <br> Docente: Ing. Maria Hurtado
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Christopher Arce Diaz <i
                                    class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i
                                    class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2020 Christopher Arce Diaz</div>
        </footer>
    </div>

    <!-- <script src="../js/busqueda.js"></script> -->
    <script src="../js/password.js"></script>
</body>

</html>