<?php
    require_once("../Model/Conexion.php");
    require_once("../Model/DocumentoTesis.php");
    // require_once("../Model/DocumentoTesis.php");
    // require_once("../Controller/LNListaTesis.php");

    $objetoDocumentoTesis = new DocumentoTesis();
    // $objetoDocumentoTesis = new LNListaTesis();

    $existeDatosTesis = $objetoDocumentoTesis->informacionDocumentoTesisTitulo($_REQUEST['infor']);


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Usuario</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <!-- <link rel="stylesheet" href="../css/texto.css"> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>

    <!-- filtro de busqueda -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>


</head>

<body>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>
                <!-- <p class="text-center" style="padding-top: 15px;">Sistema Bibliotecario</p> -->
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    <li><a href="../index.html"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li><a href="View/IUListaTesis.php"><i
                                class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Tesis</a></li>
                </ul>

                </ul>
                </li>

                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a</span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Informacion de la Tesis</small></h1>

            </div>
        </div>


        <!-- IMPLEMENTACION LISTA INFORMACION TESIS  -->


        <div class="container">
            <br>
            <div class="row">
                <div class="cold-md-5">
                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentacion" class="active"><a href="#seccion1" aria-controls="seccion1"
                                    data-toggle="tab" role="tab">Datos Generales</a></li>
                            <li role="presentacion"><a href="#seccion5" aria-controls="seccion5" data-toggle="tab"
                                    role="tab">Fotografia Tutor</a></li>
                            <li role="presentacion"><a href="#seccion2" aria-controls="seccion2" data-toggle="tab"
                                    role="tab">Resumen</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="seccion1">

                                <div class="card bg-info" style="width: 60rem;">
                                    <?php
                                    foreach($existeDatosTesis as $informacionTesis ): ?>

                                    <img src="<?php echo $informacionTesis['TapaTesis'] ?>"
                                        style="float:right;width:400px;height:300px;">

                                    <?php
                              
                                            break; 
                                            
                                                endforeach;
                        
                                    foreach($existeDatosTesis as $informacionTesis ):  ?>

                                    <h3>
                                        <p>Autor: <?php echo $informacionTesis['Autor'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Titulo: <?php echo $informacionTesis['Titulo'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Año: <?php echo $informacionTesis['Anio'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Tipo de Bibliografía: <?php echo $informacionTesis['TipoBibliografia'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Facultad: <?php echo $informacionTesis['Facultad'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Carrera: <?php echo $informacionTesis['Carrera'] ?></p>
                                    </h3>
                                    <h3>
                                        <p>Codigo: <?php echo $informacionTesis['Codigo'] ?></p>
                                    </h3>

                                    <?php
                                                // break; 
                                                
                                                    endforeach;
                                        ?>

                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="seccion2">
                                <h2>Resumen</h2><br>
                                <?php
                                        foreach($existeDatosTesis as $informacionTesis ):  ?>

                                <p class="resumen"><?php echo $informacionTesis['Resumen'] ?></p>

                                <?php
                                                    break; 
                                                    
                                                        endforeach;
                                        ?>
                            </div>


                            <div role="tabpanel" class="tab-pane" id="seccion5">
                                <div class="card bg-info" style="width: 60rem;">
                                    <?php
                                
                                foreach($existeDatosTesis as $informacionTesis ):  ?>
                                    <h2 class="card-text">Autor</h2>
                                    <img src="<?php echo $informacionTesis['fotografia'] ?>"
                                        style="float:right;width:400px;height:300px;">

                                    <h2 class="card-text">Tutor
                                        <img src="<?php echo $informacionTesis['foto'] ?>"
                                            style="float:left;width:300px;height:300px;">
                                    </h2>

                                    <?php
                                            break; 
                                            
                                                endforeach;
                                ?>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <!-- FINAL IMPLEMENTACION LISTA DOCUMENTOS TESIS  -->

        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                    </div>
                    <div class="modal-body">
                        En caso de tener algun problema, comunicarse a: christopherarce2357@gmail.com
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i
                                class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Proyecto para la materia de Base de Datos II.
                            <br> Docente: Ing. Maria Hurtado
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrollador</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Christopher Arce Diaz <i
                                    class="zmdi zmdi-facebook zmdi-hc-fw footer-social"></i><i
                                    class="zmdi zmdi-twitter zmdi-hc-fw footer-social"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">© 2020 Christopher Arce Diaz</div>
        </footer>
    </div>

    <!-- <script src="../js/busqueda.js"></script> -->
    <script type="text/javascript" src="../js/filtroBusquedas.js"></script>
</body>

</html>