<?php
        require_once("../Model/Conexion.php");
        require_once("../Model/Persona.php");
        require_once("../Model/Rol.php");
        
        $objPersona = new Persona();
        $listaDePersonas = $objPersona->listaDePersonas();
        
        $objetoRol = new Rol();
        $listaRol = $objetoRol->listaRol();

        // $objetoAsignacionCarrera = $_REQUEST['idUltimo'];

        
        // $objetoAsignacionCarrera = new AsignacionCarrera();


        // $registrarAsignacionCarrera = $objetoAsignacionCarrera->registrarAsignacionCarrera();

        date_default_timezone_set('America/La_Paz');  
        $fechaActual = date('Y-m-d H:i:s');
    // exit;

        // $datosPersona = unserialize($_GET["datosPersona"]);

?>


<!DOCTYPE html>
<html lang="es">

<head>
    <title>Registrar Tesis</title>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8"> -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/book.ico" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')
    </script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
</head>

<body>
    <?php 
    session_start();
      if(!isset($_SESSION['usuario']))
      {
        header('location:../Login.php');
      }

      if((time() - $_SESSION['last_time']) > 180) // Time in Seconds
      {
        // header("location:../Controller/CerrarSesion.php");
        // header("location:Logout.php/");
     
        require_once("../Controller/CerrarSesion.php");
 
      }
      
  ?>
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button"
                    style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i>
                sistema bibliotecario
            </div>
            <div class="full-reset" style="background-color:#2B3D51; padding: 10px 0; color:#fff;">
                <figure>
                    <img src="../assets/img/logo.png" alt="Biblioteca" class="img-responsive center-box"
                        style="width:55%;">
                </figure>

            </div>
            <div class="full-reset nav-lateral-list-menu">
            <ul class="list-unstyled">
                    <li><a href="home.php"><i class="zmdi zmdi-home zmdi-hc-fw"></i>&nbsp;&nbsp; Inicio</a></li>

                    <li>
                         <li><a href="IUPerfil.php?idPerfil=<?php echo $_REQUEST['idPersona'];?>"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i>&nbsp;&nbsp;Mi Perfil</a></li>
                         <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li>
                         <li><a href="IUListaTesisAdmi.php"><i class="zmdi zmdi-bookmark-outline zmdi-hc-fw"></i>&nbsp;&nbsp; Lista de Tesis</a></li>
                        
                    </li>
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-balance zmdi-hc-fw"></i>&nbsp;&nbsp; Registros <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                        <li><a href="IURegistrarPersona.php"><i class="zmdi zmdi-book zmdi-hc-fw"></i>&nbsp;&nbsp; Registro Tesis Proceso Completo</a></li>
                            <!-- <li><a href="IUListaPersona.php"><i class="zmdi zmdi-accounts zmdi-hc-fw"></i>&nbsp;&nbsp;Lista de Usuarios</a></li> -->
                            <li><a href="IURegistrarUsuario.php"><i class="zmdi zmdi-account-add zmdi-hc-fw"></i>&nbsp;&nbsp; Nuevo Usuario</a></li>
                        </ul>
                    </li>

                    <li><a href="IUReporteFacultad.php"><i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>&nbsp;&nbsp; Reportes y estadísticas</a></li>
                
                </ul>
            </div>
        </div>
    </div>
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                    <img src="../assets/img/user01.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">Bienvenido/a Administrador: <?php 
                        //   echo $datosPersona['primerNombre']." ".$datosPersona['primerApellido'];
                        ?></span>
                </li>
                <li class="tooltips-general exit-system-button" data-href="../index.html" data-placement="bottom"
                    title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="tooltips-general search-book-button" data-href="searchbook.html" data-placement="bottom"
                    title="Buscar libro">
                    <i class="zmdi zmdi-search"></i>
                </li>
                <li class="tooltips-general btn-help" data-placement="bottom" title="Ayuda">
                    <i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
                <h1 class="all-tittles">Sistema bibliotecario <small>Añadir Usuario</small></h1>
            </div>
        </div>
        <div class="container-fluid" style="margin: 50px 0;">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <img src="../assets/img/user01.png" alt="pdf" class="img-responsive center-box"
                        style="max-width: 110px;">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Bienvenido a la sección para agregar nueva persona a la biblioteca, deberas de llenar todos los
                    campos para poder registrar la persona
                </div>
            </div>
        </div>
        <div class="container-fluid">


            <form autocomplete="off" method="post" action="../Controller/LNRegistrarUsuario.php"
                enctype="multipart/form-data">
                <div class="container-flat-form">
                    <div class="title-flat-form title-flat-blue">Nuevo Usuario</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">


                            <!-- TABLA ASIGNACIONCARRERA -->
                            <div class="group-material">
                                <span>Rol</span>
                                <select name="idRol" class="tooltips-general material-control" data-toggle="tooltip"
                                    data-placement="top" title="Elige la Tipo de Tesis del libro">
                                    <option value="" disabled="" selected="">Selecciona un Tipo de Rol</option>
                                    <?php foreach($listaRol as $rol){ ?>
                                    <option value='<?php echo $rol['idRol'];?>'><?php echo $rol['nombre'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="group-material">
                                <input name="ci" type="number" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe tu CI, solamente números">
                                <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>CI</label>
                            </div>
                            <div class="group-material">
                                <input name="primerNombre" type="text" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe el código de la Tesis, solamente letras">
                                <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>primerNombre</label>
                            </div>
                            <div class="group-material">
                                <input name="segundoNombre" type="text" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe el código de la Tesis, solamente letras">
                                <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>segundoNombre</label>
                            </div>
                            <div class="group-material">
                                <input name="primerApellido" type="text" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe el código de la Tesis, solamente letras">
                                <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>primerApellido</label>
                            </div>
                            <div class="group-material">
                                <input name="segundoApellido" type="text" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu Cedula de Identidad" data-toggle="tooltip"
                                    data-placement="top" title="Escribe el código de la Tesis, solamente letras">
                                <!-- <input name="codigoTesis" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el código de la Tesis del libro" pattern="[0-9]{1,20}" required="" maxlength="20" data-toggle="tooltip" data-placement="top" title="Escribe el código de la Tesis, solamente números"> -->
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>segundoApellido</label>
                            </div>
                            <div class="group-material">
                                <input name="telefono" type="number" class="tooltips-general material-control"
                                    placeholder="Escribe aquí tu telefono" data-toggle="tooltip" data-placement="top"
                                    title="Escribe tu numero de telefono, solamente números">
                                <!-- <input name="resumen" type="text" class="tooltips-general material-control" placeholder="Escribe aquí el Resumen de la Tesis" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Escribe el Resumen de la Tesis"> -->
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Telefono</label>
                            </div>
                            <div class="group-material">
                                <!-- <input name="tapaTesis" type="file" class="tooltips-general material-control" placeholder="Inserta la Imagen de la Tapa de la Tesis" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Inserta la Imagen de la Tapa de la Tesis"> -->
                                <input name="file" type="file" class="tooltips-general material-control">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Fotografia</label>
                            </div>
                            <!-- <div class="group-material">
                                <input name="usuaraio" type="text" class="tooltips-general material-control" placeholder="Escribe aquí tu usuario" data-toggle="tooltip" data-placement="top" title="Escribe el título de la Tesis">
                            
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Usuario</label>
                            </div> -->
                            <!-- <div class="group-material">
                                <input name="contrasenia" type="text" class="tooltips-general material-control" placeholder="Escribe aquí tu contrasenia" data-toggle="tooltip" data-placement="top" title="Escribe el título de la Tesis">
                               
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Contraseña</label>
                            </div> -->
                            <div class="group-material">
                                <input name="fechaRegistro" type="hidden" class="tooltips-general material-control"
                                    value="<?= $fechaActual?>">
                                <span class="highlight"></span>
                                <span class="bar"></span>

                            </div>

                            <div class="group-material">
                                <input name="fechaActualizacion" type="hidden" class="tooltips-general material-control"
                                    value="<?= $fechaActual?>">
                                <span class="highlight"></span>
                                <span class="bar"></span>

                            </div>


                            <p class="text-center">
                                <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i
                                        class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                <button type="submit" class="btn btn-primary"><i class="zmdi zmdi-floppy"></i>
                                    &nbsp;&nbsp; Guardar</button>
                            </p>
                        </div>
                    </div>
                </div>
            </form>

        </div>


        <?php
           include 'footer.php'
           ?>

</body>

</html>