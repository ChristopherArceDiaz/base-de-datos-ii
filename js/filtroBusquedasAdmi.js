let facultad = 0;
let carrera = 0;
let tipoTesis = 0;
let anio = 0;
let busqueda = 0;
// let busqueda = "";

findAll();  

function ShowFacultad() {
  /* Para obtener el valor */
  // var cod = document.getElementById("producto").value;
  // alert(cod);

  /* Para obtener el texto */
  var combo = document.getElementById("facultad");
  facultad = combo.options[combo.selectedIndex].value; // trae al quien esta selecionado en el select
  console.log("facultad:", combo.options[combo.selectedIndex].value);
  findAll();
}

// function ShowCarrera()
// {
// /* Para obtener el valor */
// // var cod = document.getElementById("producto").value;
// // alert(cod);

// /* Para obtener el texto */
// var combo = document.getElementById("carrera");
// var selected = combo.options[combo.selectedIndex].text;
// console.log(selected);
// }

function ShowCarrera() {
  /* Para obtener el valor */
  // var cod = document.getElementById("producto").value;
  // alert(cod);

  /* Para obtener el texto */
  var combo = document.getElementById("carrera");
  carrera = combo.options[combo.selectedIndex].value;
  console.log(carrera);
  findAll();
}

function ShowTipoTesis() {
  /* Para obtener el valor */
  // var cod = document.getElementById("producto").value;
  // alert(cod);

  /* Para obtener el texto */
  var combo = document.getElementById("tipoTesis");
  tipoTesis = combo.options[combo.selectedIndex].value;
  console.log(tipoTesis);
  findAll();
}

function ShowAnio() {
  /* Para obtener el valor */
  // var cod = document.getElementById("producto").value;
  // alert(cod);

  /* Para obtener el texto */
  var combo = document.getElementById("anio");
  anio = combo.options[combo.selectedIndex].value;
  console.log(anio);
  findAll();
}

// function ShowTexto() {
//   /* Para obtener el valor */
//   // var cod = document.getElementById("producto").value;
//   // alert(cod);

//   /* Para obtener el texto */
//   var combo = document.getElementById("busqueda");
//   busqueda = combo.options[combo.selectedIndex].text;
//   console.log(busqueda);
//   findAll();
// }

function findAll() {
    // $("#busquedaTitulo").DataTable();
  const data = {
    facultad,
    carrera,
    tipoTesis,
    anio,
    busqueda,
  };
  console.log("form data:", data);
  $.ajax({
    url: "../Controller/filtroBusquedasAdmi.php",
    data: data,
    type: "POST",
    dataType: "json",
    success: function (response) {
      //   console.log(response);

      showTable(response);
    },
  });
}

function showTable(data) {
  //id= tBodyData
  let html = "   ";
  console.log(data);
  //   data.
  //   $.each(data, function (key, value) {
  jQuery.each(data, function (key, value) {
    html += `<tr>
        <th scope="row">${key + 1}</th>
        <td WIDTH="150">${value.Codigo}</td>
        <td WIDTH="200">${value.Autor}</td>
        <td WIDTH="650"><a href="IUListaTesisInformacionAdmi.php?infor=${value.idDocumentoTesis}">${value.Titulo}</a></td>
        <td WIDTH="150">${value.Nombre}</td>
        <td><button class="btn btn-success" type='submit'><a
        href="IUActualizarTesis.php?infor=${value.idDocumentoTesis}"><i
            class="zmdi zmdi-refresh"></i></a></button></td>
       

            <td><button class="btn btn-danger" type='submit'><a
            href="IUActualizarTesis.php?infor=${value.idDocumentoTesis}"><i
                class="zmdi zmdi-refresh"></i></a></button></td>
        </tr>
        `;
        
    console.log(value);
  });
  html += ``;
  // console.log(html);
//   $("#busquedaTitulo").DataTable();
  $("#tBodyData").html(html);
  $("#busquedaTitulo").DataTable();

  // $("#busqueda").keyup(function () {
  //   let busqueda = $("#busqueda").val();
  //   console.log(busqueda);
  //   findAll();
  // });

 
  // $("#nombre").keyup(function () {
  //   let nombre = $("#nombre").val();
  //   console.log(nombre);
  //   // findAll();
  // });
  
}
$("#busqueda").on('change keyup',function (e) {
  console.log(e.target.value)
  if(e.target.value.length>0){
    busqueda = e.target.value;
  }else{
    busqueda=0;
  }
  console.log(busqueda);
  findAll();
});
