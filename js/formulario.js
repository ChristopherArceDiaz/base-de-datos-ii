const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');

const expresiones = {
	ci: /^\d{4,8}$/, // Letras, numeros, guion y guion_bajo
    primerNombre: /^[a-zA-ZÀ-ÿ]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    segundoNombre: /^[a-zA-ZÀ-ÿ]{1,40}$/,
    primerApellido: /^[a-zA-ZÀ-ÿ]{1,40}$/,
    segundoApellido: /^[a-zA-ZÀ-ÿ]{1,40}$/,
	// password: /^.{4,12}$/, // 4 a 12 digitos.
	// correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
	telefono: /^\d{5,10}$/ // 7 a 14 numeros.
}

const campos = {
	ci: false,
	primerNombre: false, // Letras y espacios, pueden llevar acentos.
    segundoNombre: false,
    primerApellido: false,
    segundoApellido: false,
	// password: false,
	// correo: false,
	telefono: false
}

const validarFormulario = (e) => {
    // console.log(e.target.name);
	switch (e.target.name) {
		case "ci":
			validarCampo(expresiones.ci, e.target, 'ci');
			
            // console.log("Funciona");
		break;
		// case "nombre":
		// 	validarCampo(expresiones.nombre, e.target, 'nombre');
        // break;
        case "primerNombre":
			validarCampo(expresiones.primerNombre, e.target, 'primerNombre');
        break;
        case "segundoNombre":
			validarCampo(expresiones.segundoNombre, e.target, 'segundoNombre');
        break;
        case "primerApellido":
			validarCampo(expresiones.primerApellido, e.target, 'primerApellido');
        break;
        case "segundoApellido":
			validarCampo(expresiones.segundoApellido, e.target, 'segundoApellido');
		break;
		case "telefono":
			validarCampo(expresiones.telefono, e.target, 'telefono');
		break;
	}
	// console.log(e.target.name);
}

const validarCampo = (expresion, input, campo) => {
	if(expresion.test(input.value)){
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos[campo] = true;
	} else {    
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos[campo] = false;
	}
}

// // const validarPassword2 = () => {
// // 	const inputPassword1 = document.getElementById('password');
// // 	const inputPassword2 = document.getElementById('password2');

// // 	if(inputPassword1.value !== inputPassword2.value){
// // 		document.getElementById(`grupo__password2`).classList.add('formulario__grupo-incorrecto');
// // 		document.getElementById(`grupo__password2`).classList.remove('formulario__grupo-correcto');
// // 		document.querySelector(`#grupo__password2 i`).classList.add('fa-times-circle');
// // 		document.querySelector(`#grupo__password2 i`).classList.remove('fa-check-circle');
// // 		document.querySelector(`#grupo__password2 .formulario__input-error`).classList.add('formulario__input-error-activo');
// // 		campos['password'] = false;
// // 	} else {
// // 		document.getElementById(`grupo__password2`).classList.remove('formulario__grupo-incorrecto');
// // 		document.getElementById(`grupo__password2`).classList.add('formulario__grupo-correcto');
// // 		document.querySelector(`#grupo__password2 i`).classList.remove('fa-times-circle');
// // 		document.querySelector(`#grupo__password2 i`).classList.add('fa-check-circle');
// // 		document.querySelector(`#grupo__password2 .formulario__input-error`).classList.remove('formulario__input-error-activo');
// // 		campos['password'] = true;
// // 	}
// // }

inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
 	e.preventDefault();

	const terminos = document.getElementById('terminos');
	if(campos.ci && campos.primerNombre && campos.segundoNombre && campos.primerApellido && campos.segundoApellido && campos.telefono && terminos.checked ){
		formulario.reset();

		// document.formulario.submit();
		// alert("Exito");
		// enviar_formulario();
		
		// document.getElementById("formulario").submit();

		// document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
		// setTimeout(() => {
		// 	document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
		// }, 5000);

		// document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
		// 	icono.classList.remove('formulario__grupo-correcto');
		// });
		// formulario.submit();
	} 
	// else if (campos.ci && campos.primerNombre && campos.primerApellido && campos.segundoApellido && campos.telefono && terminos.checked ){
	// 	formulario.reset();
		
	// 	// document.formulario.submit();
	// 		alert("Exito");
	// 	document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
	// 	setTimeout(() => {
	// 		document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
	// 	}, 5000);

	// 	document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
	// 		icono.classList.remove('formulario__grupo-correcto');
	// 	});
	// 	formulario.submit();
	// }
	// else if (campos.ci && campos.primerNombre && campos.primerApellido && campos.telefono && terminos.checked ){
	// 	formulario.reset();

	// 	this.submit();
	// 	// document.formulario.submit();
	// 		alert("Exito");
	// 	document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
	// 	setTimeout(() => {
	// 		document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
	// 	}, 5000);

	// 	document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
	// 		icono.classList.remove('formulario__grupo-correcto');
	// 	});
	// 	formulario.submit();
	// }
	else {
		// document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
		alert("Error");
			
	}
});

// function enviar_formulario(){
//    document.formulario.submit()
// }
