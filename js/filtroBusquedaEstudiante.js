let facultad = 0;
let carrera = 0;
let tipoTesis = 0;
let fecha = 0;
let general = 0;

findAll();

function ShowFacultad() {
  /* Para obtener el valor */
  // var cod = document.getElementById("producto").value;
  // alert(cod);

  /* Para obtener el texto */
  var combo = document.getElementById("facultad");
  facultad = combo.options[combo.selectedIndex].value; // trae al quien esta selecionado en el select
  console.log("facultad:", combo.options[combo.selectedIndex].value);
  findAll();
}

// function ShowCarrera()
// {
// /* Para obtener el valor */
// // var cod = document.getElementById("producto").value;
// // alert(cod);

// /* Para obtener el texto */
// var combo = document.getElementById("carrera");
// var selected = combo.options[combo.selectedIndex].text;
// console.log(selected);
// }

function ShowCarrera() {
  /* Para obtener el valor */
  // var cod = document.getElementById("producto").value;
  // alert(cod);

  /* Para obtener el texto */
  var combo = document.getElementById("carrera");
  carrera = combo.options[combo.selectedIndex].value;
  console.log(carrera);
  findAll();
}

function ShowTipoTesis() {
  /* Para obtener el valor */
  // var cod = document.getElementById("producto").value;
  // alert(cod);

  /* Para obtener el texto */
  var combo = document.getElementById("tipoTesis");
  tipoTesis = combo.options[combo.selectedIndex].value;
  console.log(tipoTesis);
  findAll();
}

function findAll() {
    // $("#busquedaTitulo").DataTable();
  const data = {
    facultad,
    carrera,
    tipoTesis,
    fecha,
    general,
  };
  console.log("form data:", data);
  $.ajax({
    url: "../Controller/filtroBusquedasDocentes.php",
    data: data,
    type: "POST",
    dataType: "json",
    success: function (response) {
      //   console.log(response);

      showTable(response);
    },
  });
}

function showTable(data) {
  //id= tBodyData
  let html = "   ";
  console.log(data);
  //   data.
  //   $.each(data, function (key, value) {
  jQuery.each(data, function (key, value) {
    html += `<tr>
        <th scope="row">${key + 1}</th>
        <td WIDTH="150">${value.Codigo}</td>
        <td WIDTH="200">${value.Autor}</td>
        <td WIDTH="650">${value.Titulo}</td>
        <td WIDTH="150">${value.Nombre}</td>
        </tr>
        `;
        
    console.log(value);
  });
  html += ``;
//   console.log(html);
//   $("#busquedaTitulo").DataTable();
  $("#tBodyData").html(html);
//   $("#busquedaTitulo").DataTable();

  $("#fecha").keyup(function () {
    let search = $("#fecha").val();
    console.log(search);
    findAll();
  });
}
