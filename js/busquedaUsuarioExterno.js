  $(document).ready(function(){
    $.ajax({
      type: 'POST',
      url: '../Controller/cargar_facultades.php'
    })
    .done(function(listas_rep){
      $('#facultad').html(listas_rep)
    })
    .fail(function(){
      alert('Hubo un errror al mostrar las facultades')
    })
  
    $('#facultad').on('change', function(){
      var id = $('#facultad').val()
    //   alert(id);
      $.ajax({
        type: 'POST',
        url: '../Controller/cargar_carreras.php',
        data: {'id': id}
      })
      .done(function(listas_rep){
        $('#carrera').html(listas_rep)
      })
      .fail(function(){
        alert('Hubo un errror al mostrar las carreras')
      })
    })
  
    // $('#enviar').on('click', function(){
    //   var resultado = 'Lista de reproducción: ' + $('#lista_reproduccion').text() +
    //   ' Video elegido: ' + $('#carrera').text()
  
    //   $('#resultado1').html(resultado)
    // })
  
  })