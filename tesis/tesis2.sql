DROP DATABASE IF EXISTS tesis2;
CREATE DATABASE tesis2;
USE tesis2
-- TABLAS PRIMARIAS
-- Carlos
CREATE TABLE universidad (
idUniversidad INT PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(35) NOT NULL,
direccion VARCHAR(75),
telefono VARCHAR(15),
logo VARCHAR(400) NOT NULL
)ENGINE=InnoDB;

-- Crear Facultad
CREATE TABLE facultad(
idFacultad INT PRIMARY KEY AUTO_INCREMENT,
idUniversidad INT NOT NULL,
nombre VARCHAR(50),
sigla VARCHAR(5),
FOREIGN KEY(idUniversidad) REFERENCES universidad(idUniversidad) 
)ENGINE=InnoDB;

-- Jose Maria
CREATE TABLE rol(
idRol INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(30) NOT NULL
)ENGINE=InnoDB;

-- Kevin Rod
CREATE TABLE tipotesis(
idTipoTesis INT UNSIGNED NOT NULL  AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(25) NOT NULL
)ENGINE=InnoDB;


-- Cristofer
CREATE TABLE rolPersonalTesis(
idRolPersonalTesis INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(15) NOT NULL
)ENGINE=InnoDB;
	
-- TABLAS RELACIONALES
-- Edson
CREATE TABLE carrera (
idCarrera INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
idFacultad  INT NOT NULL,
nombre VARCHAR(55) NOT NULL,
sigla VARCHAR(5) NOT NULL,
FOREIGN KEY(idFacultad) REFERENCES facultad(idFacultad)
)ENGINE = InnoDB;

-- Jose Zapata
create table persona (
idPersona INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
idRol INT UNSIGNED NOT NULL,
ci INT (10) NOT NULL,
primerNombre VARCHAR (15) NOT NULL,  
segundoNombre VARCHAR (15),
primerApellido VARCHAR (15) NOT NULL,
segundoApellido VARCHAR (15),
telefono INT (10) NOT NULL,
fotografia LONGTEXT, -- Pensar estudiantes el tipo
activo boolean NOT NULL DEFAULT 1,
usuario VARCHAR (50) NOT NULL,
contrasenia longblob NOT NULL,
fechaRegistro DATETIME NOT NULL,
fechaActualizacion DATETIME NOT NULL,
FOREIGN KEY (idRol) REFERENCES rol(idRol)
)ENGINE=InnoDB;

-- Neftali
-- CREATE TABLE personalTesis(
-- idPersonalTesis INT AUTO_INCREMENT PRIMARY KEY,
-- ci VARCHAR(10) UNIQUE NOT NULL,
-- primerNombre VARCHAR(15)NOT NULL,
-- segundoNombre VARCHAR(15),
-- apellidoPaterno VARCHAR(15)NOT NULL,
-- apellidoMaterno VARCHAR(15),
-- fotografia LONGTEXT,
-- activo boolean NOT NULL DEFAULT 1
-- )ENGINE=InnoDB;

    CREATE TABLE personalTesis(
    idPersonalTesis INT AUTO_INCREMENT PRIMARY KEY,
    idRolPersonalTesis INT UNSIGNED NOT NULL,
    ci VARCHAR(10) UNIQUE NOT NULL,
    primerNombre VARCHAR(15)NOT NULL,
    segundoNombre VARCHAR(15),
    apellidoPaterno VARCHAR(15)NOT NULL,
    apellidoMaterno VARCHAR(15),
    fotografia VARCHAR (150),
    activo boolean NOT NULL DEFAULT 1,
    FOREIGN KEY (idRolPersonalTesis) REFERENCES rolPersonalTesis(idRolPersonalTesis) 
    )ENGINE=InnoDB; 

-- FAUSTO
create table asignacionCarrera(
idAsignacionCarrera INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
idCarrera INT UNSIGNED NOT NULL,
idPersona INT UNSIGNED NOT NULL,
FOREIGN KEY(idCarrera) REFERENCES carrera(idCarrera),
FOREIGN KEY(idPersona) REFERENCES persona(idPersona)
)ENGINE=InnoDB;

-- DILAN Y DOUGLAS
CREATE TABLE documentoTesis(
idDocumentoTesis INT AUTO_INCREMENT PRIMARY KEY,
idTipoTesis INT UNSIGNED NOT NULL,
codigoTesis VARCHAR (30) UNIQUE NOT NULL,
fechaHoraRegistro DATETIME NOT NULL,
titulo VARCHAR(200),
resumen LONGTEXT,
introduccion LONGTEXT,
palabrasClave VARCHAR(100),
imagenTapaTesis VARCHAR(200),
documentoCompleto VARCHAR(200),
FOREIGN KEY(idTipoTesis) REFERENCES tipoTesis(idTipoTesis)
)ENGINE=InnoDB;

-- RODRIGO
-- CREATE TABLE participantesTesis (
-- idDocumentoTesis INT NOT NULL,
-- idPersonalTesis INT NOT NULL,
-- idRolPersonalTesis INT UNSIGNED NOT NULL,
-- FOREIGN KEY(idDocumentoTesis) REFERENCES documentoTesis(idDocumentoTesis),
-- FOREIGN KEY(idPersonalTesis) REFERENCES personalTesis(idPersonalTesis),
-- FOREIGN KEY(idRolPersonalTesis) REFERENCES rolPersonalTesis(idRolPersonalTesis)
-- )ENGINE=InnoDB;

    CREATE TABLE participantesTesis (
    idDocumentoTesis INT NOT NULL,
    idPersonalTesis INT NOT NULL,
    FOREIGN KEY(idDocumentoTesis) REFERENCES documentoTesis(idDocumentoTesis),
    FOREIGN KEY(idPersonalTesis) REFERENCES personalTesis(idPersonalTesis)
    )ENGINE=InnoDB;

CREATE TABLE autor(
idDocumentoTesis INT NOT NULL,
idAsignacionCarrera INT UNSIGNED NOT NULL,
FOREIGN KEY(idDocumentoTesis) REFERENCES documentoTesis(idDocumentoTesis),
FOREIGN KEY(idAsignacionCarrera) REFERENCES asignacionCarrera(idAsignacionCarrera)
)ENGINE=InnoDB; 






-- SELECT p.idPersonalTesis AS idPersonalTesis, CONCAT_WS(' ', p.apellidoPaterno, p.apellidoMaterno,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci
-- FROM personalTesis p INNER JOIN participantesTesis pt ON p.idPersonalTesis = pt.idPersonalTesis
-- ORDER BY p.apellidoPaterno;

-- SELECT DISTINCT p.idPersonalTesis AS idPersonalTesis, CONCAT_WS(' ', p.apellidoPaterno, p.apellidoMaterno,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado
-- FROM personalTesis p INNER JOIN participantesTesis pt ON p.idPersonalTesis = pt.idPersonalTesis
-- INNER JOIN rolPersonalTesis r ON pt.idRolPersonalTesis = r.idRolPersonalTesis
-- ORDER BY p.apellidoPaterno;


--             SELECT p.idPersona AS idPersona, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, p.fechaRegistro AS fechaRegistro
--             FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
--             ORDER BY p.primerApellido;

            
---------------------------------------------------------------------------
---------------------------------------------------------------------------
,  (SELECT dt.titulo) AS Titulo
            SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT(p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE dt.idDocumentoTesis = 5
            GROUP BY dt.idDocumentoTesis
            ORDER BY dt.idDocumentoTesis;
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
---------------------------------------------------------------------------
---------------------------------------------------------------------------
(SELECT dt.titulo) AS Titulo

            SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, GROUP_CONCAT(DISTINCT ' ', p.primerApellido, ' ', p.segundoApellido, ' ', p.primerNombre, ' ', p.segundoNombre) AS Autor, GROUP_CONCAT(' ', pert.apellidoPaterno, ' ', pert.apellidoMaterno, ' ', pert.primerNombre, ' ', pert.segundoNombre) AS Tutor 
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE dt.idDocumentoTesis = 1
            GROUP BY dt.idDocumentoTesis
            ORDER BY dt.idDocumentoTesis;



-- , tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha

-- WHERE dt.idDocumentoTesis = 1
-- ORDER BY dt.fechaHoraRegistro DESC

--             SELECT count(idAsignacionCarrera) FROM asignacionCarrera;
--             SELECT count(idPersona) FROM persona WHERE idRol = 3;
--             SELECT * FROM persona WHERE idRol = 3;

-- SELECT fac.nombre, GROUP_CONCAT(ca.nombre)
-- FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
-- GROUP BY fac.nombre;


--             SELECT ca.nombre AS Carrera, fac.nombre AS Facultad
--             FROM carrera ca INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
--             GROUP BY ca.nombre;
    