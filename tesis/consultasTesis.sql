--La persona (que es un usuario externo) que realiza las búsquedas podrá solamente ver algunos datos de su
--tesis como: código de la tesis (por si el usuario va de manera física a la biblioteca y quiera revisar el
--documento), Autor(es) de la Tesis (Apellido Paterno, Apellido Materno, Primer Nombre, Segundo Nombre, Si
--hay otro autor colocar el otro seguido de un coma), titulo, Tipo de Bibliografía (Proyecto de Grado, Tesis,
--Trabajo Dirigido, etc.). De cada búsqueda que realiza el usuario, debe mostrarse de 10 en 10 hasta el final.
--Mostrar el orden de las tesis de los más actuales hasta el mas antiguo.
--9

SELECT dt.idDocumentoTesis, dt.codigoTesis, CONCAT_WS(' ', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
AND r.idRol = '3'
ORDER BY dt.fechaHoraRegistro DESC
LIMIT 1,10; 





SELECT dt.idDocumentoTesis AS idDocumentoTesis, dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
FROM documentoTesis dt INNER JOIN participantesTesis pt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN persona p ON p.idPersona = pt.idPersona
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
ORDER BY dt.fechaHoraRegistro DESC
LIMIT 1,10;


--El titulo debe ser un enlace, y cuando el usuario realice un click sobre este, deberá mostrar más detalle como:
--(Tome en cuenta que usted debe darle un buen diseño a mostrar estos datos)

--Autor (es): Titulo: Fecha: Tipo de Bibliografía: Facultad: Carrera: Resumen: Código de la Tesis: Tapa de la Tesis:
--10
SELECT CONCAT_WS('', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, dt.fechaHoraRegistro AS Fecha, tT.nombre AS TipoBibliografia, fac.nombre AS Facultad, ca.nombre AS Carrera, dt.resumen AS Resumen,dt.idDocumentoTesis AS Codigo, dt.imagenTapaTesis AS TapaTesis
FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
INNER JOIN persona p ON p.idPersona = am.idPersona
INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
AND r.idRol = '3'
AND dt.idDocumentoTesis = 8
ORDER BY fac.nombre DESC;


--Se requiere que las búsquedas sean dinámicas y que tenga diferentes filtros como:
--Titulo: dentro de este campo el usuario puede colocar todo el titulo o parte del título y se debe realizar la
--búsqueda con las que coincida. Ejemplo: Aplicación Web Progresiva para gestión de ventas.

--11
DROP PROCEDURE IF EXISTS SP_Titulo;
DELIMITER //
CREATE PROCEDURE SP_Titulo(palabra VARCHAR(25))
BEGIN
SELECT dt.titulo AS Titulo, CONCAT_WS('', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.fechaHoraRegistro AS Fecha, tT.nombre AS TipoBibliografia, fac.nombre AS Facultad, ca.nombre AS Carrera, dt.resumen AS Resumen,dt.idDocumentoTesis AS Codigo, dt.imagenTapaTesis AS TapaTesis
FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
INNER JOIN persona p ON p.idPersona = am.idPersona
INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
WHERE dt.titulo LIKE CONCAT('%', palabra,'%')
AND r.idRol = '3'
ORDER BY dt.titulo DESC;
END//
DELIMITER ;
    
CALL SP_Titulo('aplicacion');

--12

DROP PROCEDURE IF EXISTS SP_12;
DELIMITER //
CREATE PROCEDURE SP_12(palabra VARCHAR(25))
BEGIN
SELECT dt.titulo AS Titulo, CONCAT_WS('', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.fechaHoraRegistro AS Fecha, tT.nombre AS TipoBibliografia, fac.nombre AS Facultad, ca.nombre AS Carrera, dt.resumen AS Resumen,dt.idDocumentoTesis AS Codigo, dt.imagenTapaTesis AS TapaTesis
FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
INNER JOIN persona p ON p.idPersona = am.idPersona
INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
WHERE dt.titulo LIKE CONCAT('%', palabra,'%')
AND r.idRol = '3'
ORDER BY dt.titulo DESC;
END//
DELIMITER ;

CALL SP_12('ventas');

--13

DROP PROCEDURE IF EXISTS SP_13;
DELIMITER //
CREATE PROCEDURE SP_13(facultad VARCHAR(25),carrera VARCHAR(25),tipoTesis VARCHAR(25))
BEGIN
    SELECT fac.nombre AS Facultad, ca.nombre AS Carrera,tT.nombre AS Nombre, dt.idDocumentoTesis AS ID, dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, dt.fechaHoraRegistro AS Fecha
    FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
    INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
    INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
    INNER JOIN asignacionCarrera ac ON ac.idPersona = p.idPersona
    INNER JOIN carrera ca ON ac.idCarrera = ca.idCarrera
    INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
    WHERE fac.nombre LIKE CONCAT('%', facultad,'%')
    AND ca.nombre LIKE CONCAT('%', carrera,'%')
    AND tT.nombre LIKE CONCAT('%', tipoTesis,'%')
    AND r.idRol = '3'    
    ORDER BY fac.nombre DESC;
    
END//
DELIMITER ;

CALL SP_13('Ing', 'sis', 'pro');




--15

SELECT dt.idDocumentoTesis, dt.codigoTesis, CONCAT_WS(' ', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
AND r.idRol = '3'
ORDER BY dt.fechaHoraRegistro DESC

DROP PROCEDURE IF EXISTS SP_15;
DELIMITER //
CREATE PROCEDURE SP_15(anio VARCHAR(25))
BEGIN
    SELECT dt.fechaHoraRegistro AS Anio, dt.idDocumentoTesis, dt.codigoTesis, CONCAT_WS(' ', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre
    FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
    INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
    INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
    WHERE fechaHoraRegistro LIKE CONCAT('%', anio,'%')
    AND r.idRol = '3'
    ORDER BY dt.fechaHoraRegistro;
    
    
END//
DELIMITER ;

CALL SP_15('2000');



--16

SELECT CONCAT_WS('', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, dt.fechaHoraRegistro AS Fecha, tT.nombre AS 'Tipo de Bibliografia', fac.nombre AS Facultad, ca.nombre AS Carrera, dt.idDocumentoTesis AS ID, dt.imagenTapaTesis AS Imagen, p.fotografia AS Fotografia, CONCAT_WS('', perT.primerNombre, perT.segundoNombre, perT.apellidoPaterno, perT.apellidoMaterno) AS 'Personal Revisor', rolPerT.nombre AS 'Rol Revisor'
FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
INNER JOIN persona p ON p.idPersona = am.idPersona
INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN personalTesis perT ON perT.idPersonalTesis = pt.idPersonalTesis
INNER JOIN rolPersonalTesis rolPerT ON rolPerT.idRolPersonalTesis = pt.idRolPersonalTesis
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
AND dt.idDocumentoTesis = 8
ORDER BY fac.nombre DESC;



--17
DROP PROCEDURE IF EXISTS SP_17;
DELIMITER //
CREATE PROCEDURE SP_17(palabra VARCHAR(25))
BEGIN
SELECT dt.fechaHoraRegistro AS Fecha,dt.titulo AS Titulo, CONCAT_WS('', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.fechaHoraRegistro AS Fecha, tT.nombre AS TipoBibliografia, fac.nombre AS Facultad, ca.nombre AS Carrera, dt.resumen AS Resumen,dt.idDocumentoTesis AS Codigo, dt.imagenTapaTesis AS TapaTesis
FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
INNER JOIN persona p ON p.idPersona = am.idPersona
INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
WHERE dt.titulo LIKE CONCAT('%', palabra,'%')
AND r.idRol = '3'
ORDER BY dt.titulo DESC;
END//
DELIMITER ;

CALL SP_17('hel');


--20
DROP PROCEDURE IF EXISTS SP_RegistrarUsuario;
DELIMITER //
CREATE PROCEDURE SP_RegistrarUsuario(idUsuarioo INT,cii VARCHAR(12),primerNombree VARCHAR(15),segundoNombree VARCHAR(15),apellidoPaternoo VARCHAR(15),apellidoMaternoo VARCHAR(15),telefono INT, fotografia VARCHAR(15))
BEGIN
INSERT INTO persona(idUsuario,ci,primerNombre,segundoNombre,apellidoPaterno,apellidoMaterno,telefono, fotografia) 
VALUES (idUsuarioo,cii,primerNombree,segundoNombree,apellidoPaternoo,apellidoMaternoo,telefono, fotografia);
	
END//
DELIMITER ;


--21
DROP PROCEDURE IF EXISTS SP_ActualizarUsuario;
DELIMITER //
CREATE PROCEDURE SP_ActualizarUsuario(idUsuarioo INT,idRoll INT,cii VARCHAR(12),
                 primerNombree VARCHAR(15),segundoNombree VARCHAR(15),apellidoPaternoo VARCHAR(15),
                 apellidoMaternoo VARCHAR(15),telefonoFijo INT,fotografiaa VARCHAR(20),activoo bool)       
BEGIN
UPDATE usuario 
SET idRol=idRoll,ci=cii,primerNombre=primerNombree,segundoNombre=segundoNombree,
    apellidoPaterno=apellidoPaternoo,apellidoMaterno=apellidoMaternoo,
    telefono=telefonoFijo,fotografia=fotografiaa,activo = activoo,fechaActualizacion=CURDATE()
	
WHERE idUsuario=idUsuarioo; 
		
END//
DELIMITER ;


--26
DROP PROCEDURE IF EXISTS SP_RegistrarTesis;
DELIMITER //
CREATE PROCEDURE SP_RegistrarTesis(idAsignacionCarreraa INT, idTipoTesiss INT, codigoTesiss VARCHAR(12),fechaHoraRegistroo VARCHAR(12),tituloo VARCHAR(15),resumenn VARCHAR(200),introduccionn VARCHAR(15),palabrasClavee VARCHAR(100),imagenTapaTesis VARCHAR(15), documentoCompleto VARCHAR(15))
BEGIN
INSERT INTO documentoTesis(idAsignacionCarrera,idTipoTesis,codigoTesis,fechaHoraRegistro,titulo,resumen,introduccion, palabrasClave,imagenTapaTesis,documentoCompleto) 
VALUES (idAsignacionCarreraa,idTipoTesiss,codigoTesiss,fechaHoraRegistroo,tituloo,resumenn,introduccionn, palabrasClavee,imagenTapaTesiss,documentoCompletoo);
	
END//
DELIMITER ;


--Como reporte de administrador se requiere: Por facultad cuantas tesis (todas en conjunto) se tienen
--registradas en total hasta el año actual.
--Para esta consulta debe mostrar por cada facultad más de 30 Tesis y debe variar incluso por carrera la cantidad
--de tesis y debe varias las modalidades de titulación. Si una facultad tiene mas de tres carreras entonces como
--minimo a cada carrera le asignara 10 tesis (este no es un numero exacto usted puede varias a MAS)
--NOTA: Menos de esta cantidad no será tomado en cuenta al momento de su presentación.
--Para este reporte muestre tanto el reporte en tabla con imagen estadística (Torta o Barras).
--En el reporte, muestre la fecha actual en el cual se esta generando dicho reporte.
--27
SELECT fac.nombre AS Facultad, COUNT(dt.idDocumentoTesis) AS Cantidad
FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
INNER JOIN persona p ON p.idPersona = am.idPersona
INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
AND r.idRol = '3'
GROUP BY fac.nombre
ORDER BY fac.nombre;





SELECT ca.nombre AS Carrera, COUNT(dt.idDocumentoTesis) AS Cantidad, ca.sigla AS Sigla
FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
INNER JOIN persona p ON p.idPersona = am.idPersona
INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
AND r.idRol = '3'
GROUP BY ca.nombre
ORDER BY ca.nombre;




--28
DROP PROCEDURE IF EXISTS SP_28;
DELIMITER //
CREATE PROCEDURE SP_28(fecha VARCHAR(25))
BEGIN
    SELECT dt.fechaHoraRegistro AS Fecha,dt.titulo AS Titulo,fac.nombre AS Facultad
    FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
    INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
    INNER JOIN persona p ON p.idPersona = am.idPersona
    INNER JOIN rol r ON p.idRol = r.idRol
    INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
    INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
    WHERE dt.fechaHoraRegistro LIKE CONCAT('%', fecha,'%')
    AND r.idRol = '3'
    ORDER BY fac.nombre;
    
END//
DELIMITER ;

CALL SP_28('1999');



    SELECT fac.nombre AS Facultad, ca.nombre AS Carrera,COUNT(dt.codigoTesis) AS Cantidad
    FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
    INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
    INNER JOIN persona p ON p.idPersona = am.idPersona
    INNER JOIN rol r ON p.idRol = r.idRol
    INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
    INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
    AND r.idRol = '3'
    AND fac.idFacultad = 2
    GROUP BY ca.nombre
    ORDER BY ca.nombre;
    

--29
DROP PROCEDURE IF EXISTS SP_29;
DELIMITER //
CREATE PROCEDURE SP_29(facultad VARCHAR(25))
BEGIN
    SELECT fac.nombre AS Facultad, ca.nombre AS Carrera,COUNT(dt.codigoTesis) AS Cantidad
    FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
    INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
    INNER JOIN persona p ON p.idPersona = am.idPersona
    INNER JOIN rol r ON p.idRol = r.idRol
        INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
        INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
        INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
        WHERE fac.nombre LIKE CONCAT('%', facultad,'%')
        AND r.idRol = '3'
        GROUP BY ca.nombre
        ORDER BY ca.nombre;
        
    END//
    DELIMITER ;

    CALL SP_29('Ingeni');




--30
DROP PROCEDURE IF EXISTS SP_30;
DELIMITER //
CREATE PROCEDURE SP_30(facultad VARCHAR(25),fecha VARCHAR(25))
BEGIN
    SELECT dt.fechaHoraRegistro AS Fecha,dt.titulo AS Titulo,fac.nombre AS Facultad, COUNT(dt.fechaHoraRegistro) AS Cantidad
    FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
    INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
    INNER JOIN persona p ON p.idPersona = am.idPersona
    INNER JOIN rol r ON p.idRol = r.idRol
    INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
    INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
    WHERE fac.nombre LIKE CONCAT('%', facultad,'%')
    AND dt.fechaHoraRegistro LIKE CONCAT('%', fecha,'%')
    AND r.idRol = '3'
    ORDER BY dt.fechaHoraRegistro;
    
END//
DELIMITER ;

CALL SP_30('Teologia', '2010');





--31
DROP PROCEDURE IF EXISTS SP_31;
DELIMITER //
CREATE PROCEDURE SP_31(carrera VARCHAR(25))
BEGIN
    SELECT dt.fechaHoraRegistro AS Fecha,fac.nombre AS Facultad, ca.nombre AS Carrera, COUNT(dt.idDocumentoTesis) AS Cantidad, tT.nombre AS 'Tipo de Tesis'
    FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
    INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
    INNER JOIN persona p ON p.idPersona = am.idPersona
    INNER JOIN rol r ON p.idRol = r.idRol
    INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
    INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
    WHERE ca.nombre LIKE CONCAT('%', carrera,'%')
    AND r.idRol = '3'
    GROUP BY tT.nombre
    ORDER BY dt.fechaHoraRegistro;
    
END//
DELIMITER ;

CALL SP_31('Teol');


--32
DROP PROCEDURE IF EXISTS SP_RegistrarUniversidad;
DELIMITER //
CREATE PROCEDURE SP_RegistrarUniversidad(nombree VARCHAR(12),direccionn VARCHAR(12),telefonoo INT,logoo VARCHAR(200))
BEGIN
INSERT INTO universidad(nombre,direccion,telefono,logo) 
VALUES (nombree,direccionn,direccionn,telefonoo,logoo);
	
END//
DELIMITER ;


--Consulta Fechas
    SELECT dt.fechaHoraRegistro AS Fecha,dt.titulo AS Titulo,fac.nombre AS Facultad
    FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
    INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
    INNER JOIN persona p ON p.idPersona = am.idPersona
    INNER JOIN rol r ON p.idRol = r.idRol
    INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
    INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
    WHERE YEAR(dt.fechaHoraRegistro) BETWEEN '2000' AND '2002'
    AND r.idRol = '3'
    ORDER BY fac.nombre;