<?php
 
 require_once("../Model/Conexion.php");
 require_once("../Model/DocumentoTesis.php");
 require_once("../Model/AsignacionCarrera.php");
    
    $objetoTesis = new DocumentoTesis();

    
    // echo $objetoAsignacionCarrera->registrarAsignacionCarrera->lastInsertId();

    // $idAsignacionCarrera = $_POST['idAsignacionCarrera'];
    // exit();
    // echo $_REQUEST['idCarrera'];
    // exit;
    // echo $_REQUEST['titulo'];
    // exit();
        
    $tit = $_POST['titulo'];
    $titu = strtolower($tit);
    $titulo = ucwords($titu);

    // FOTO
    $foto = $_FILES['file']['name'];
    $ruta = $_FILES['file']['tmp_name'];
    $destino = "../img/TapaTesis/".$foto;
    copy($ruta,$destino);


    $fechaActual = $_POST['fechaRegistro'];
    // $anio = date("Y", $fechaActual);

    $pdf = $_FILES['pdf']['name'];
    $rutaPDF = $_FILES['pdf']['tmp_name'];
    $destinoPDF = "../TesisPDF/".$pdf;
    copy($ruta,$destinoPDF);

    $verificarCI = $objetoTesis->verificarTituloTesis($_REQUEST['titulo']);
    // $verificarCI->rowCount();
   
    if($verificarCI > 0)
    {
        ?>
        <script>
            alert("El Titulo que ingreso ya existe");
            window.location = "../View/IURegistrarTesis.php";
        </script>

<?php
    }
    else
    {
            if($_REQUEST['idCarrera'] == 1)
            {
                $code = strtoupper("TES-FING-INSI-2020-");
                // $code2 = $code.$anio."-";
                $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $codigo = strtoupper("TES-FING-INSI-2020-40");

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 2) 
            {
                // $codigo = strtoupper("TES-FING-RETE-2020-40");

                // $code2 = $code.$anio."-";
                $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 3) 
            {
                // $codigo = strtoupper("TES-FING-INAM-2020-40");
                $code = strtoupper("TES-FING-INAM-2020-");
                // $code2 = $code.$anio."-";
                $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 4) 
            {
                $code = strtoupper("TES-FSAL-FISI-2020-");
            // $code2 = $code.$anio."-";
            $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 5) 
            {
                $code = strtoupper("TES-FSAL-BIOQ-2020-");
            // $code2 = $code.$anio."-";
            $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 6) 
            {
                $code = strtoupper("TES-FSAL-NUCI-2020-");
            // $code2 = $code.$anio."-";
            $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 7) 
            {
                $code = strtoupper("TES-FHUM-PSIC-2020-");
            // $code2 = $code.$anio."-";
            $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 8) 
            {
                $code = strtoupper("TES-FHUM-PEDA-2020-");
                // $code2 = $code.$anio."-";
                $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 9) 
            {
                $code = strtoupper("TES-FHUM-ACFI-2020-");
                // $code2 = $code.$anio."-";
                $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 10) 
            {
                $code = strtoupper("TES-FCEA-ADMI-2020-");
        // $code2 = $code.$anio."-";
        $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 11) 
            {
                $code = strtoupper("TES-FCEA-INCO-2020-");
            // $code2 = $code.$anio."-";
            $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 12) 
            {
                $code = strtoupper("TES-FCEA-INFI-2020-");
                // $code2 = $code.$anio."-";
                $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
            elseif ($_REQUEST['idCarrera'] == 13) 
            {
                $code = strtoupper("TES-FTEO-TEOL-2020-");
            // $code2 = $code.$anio."-";
            $codigo = $code.$_REQUEST['idAsignacionCarrera'];

                $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idTipoTesis'],
                $codigo,
                $fechaActual,
                $titulo,
                $_REQUEST['resumen'],
                $_REQUEST['introduccion'],
                $_REQUEST['palabrasClaves'],
                $destino,
                $destinoPDF
        );
            }
    }
 



    // $s = 'PLATA JUGUETES Y ÚTILES PLATA, Juguetes y útiles';
    // $v = titleCase($s);
    // echo $v."";
    
    // function titleCase($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("a","ante","bajo","cabe","con","contra","de","desde","en","entre","hacia","hasta","para","por","según","sin","so","sobre","tras","y"))
    // {
    //     /*
    //      * Exceptions en minusculas son las palabras que no deseas convertir
    //      * Exceptions en mayusculas son las palabras que no deseas convertir a title case
    //      *   pero deben ser converidas a mayusculas, e.g.:
    //      *   king henry viii o king henry Viii deben ser King Henry VIII
    //      */
    //     $string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
    //     foreach ($delimiters as $dlnr => $delimiter) {
    //         $words = explode($delimiter, $string);
    //         $newwords = array();
    //         foreach ($words as $wordnr => $word) {
    //             if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
    //                 // check exceptions list for any words that should be in upper case
    //                 $word = mb_strtoupper($word, "UTF-8");
    //             } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
    //                 // check exceptions list for any words that should be in upper case
    //                 $word = mb_strtolower($word, "UTF-8");
    //             } elseif (!in_array($word, $exceptions)) {
    //                 // convert to uppercase (non-utf8 only)
    //                 $word = ucfirst($word);
    //             }
    //             array_push($newwords, $word);
    //         }
    //         $string = join($delimiter, $newwords);
    //    }//foreach
    //    return $string;
    // }

    // $exitoRegistro = $objetoTesis->registrarTesis($_REQUEST['idAsignacionCarrera'],
    //                                                     $_REQUEST['idTipoTesis'],
    //                                                     $codigo,
    //                                                     $fechaActual,
    //                                                     $_REQUEST['titulo'],
    //                                                     $_REQUEST['resumen'],
    //                                                     $_REQUEST['introduccion'],
    //                                                     $_REQUEST['palabrasClaves'],
    //                                                     $destino,
    //                                                     $destinoPDF
    // );
                                                

    // if($exitoRegistro==1){
    //     echo "La tesis no se registro";
        
        // header("Location: ../View/IUListaTesisAdmi.php");

    // }
    // else
    // {
        // header("Location: ../View/IUListaTesisAdmi.php");
        // echo "Error al registrar al usuario";
    //  }
        
        
    
?>