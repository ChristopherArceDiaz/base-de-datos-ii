<?php
 
 require_once("../Model/Conexion.php");
 require_once("../Model/Persona.php");
    
    $objetoPersona = new Persona();

    $fechaActual = $_POST['fechaActualizacion'];
    
    $primerN = $_POST['primerNombre'];
    $primerNomb = strtolower($primerN);
    $primerNombre = ucfirst($primerNomb);

    $segundoN = $_POST['segundoNombre'];
    $segundoNom = strtolower($segundoN);
    $segundoNombre = ucfirst($segundoNom);

    $primerA = $_POST['primerApellido'];
    $primerApell = strtolower($primerA);
    $primerApellido = ucfirst($primerApell);

    $segundoA = $_POST['segundoApellido'];
    $segundoApell = strtolower($segundoA);
    $segundoApellido = ucfirst($segundoApell);


     // FOTO
     $foto = $_FILES['file']['name'];
     $ruta = $_FILES['file']['tmp_name'];
     $destino = "../img/fotos/".$foto;
     copy($ruta,$destino);

    // $idPersona = $existeDatosPersona['idPersona'];

    // $_REQUEST['idRol'],
    $exitoRegistro = $objetoPersona->actualizarPerfil($_REQUEST['idPersona'],
                                                    $_REQUEST['ci'],
                                                    $primerNombre,
                                                    $segundoNombre,
                                                    $primerApellido,
                                                    $segundoApellido,
                                                    $_REQUEST['telefono'],
                                                    $destino,
                                                    $_REQUEST['password'],
                                                    $fechaActual
    );
    
    // exit();
    if($exitoRegistro==1){
        
        header("Location: ../View/home.php");
    }else
    {
      
        echo "Error al actualizar al usuario";
      
     }
        
        
    
?>