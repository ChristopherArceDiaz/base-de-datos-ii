<?php
 
 require_once("../Model/Conexion.php");
 require_once("../Model/Persona.php");
    
    $objetoPersona = new Persona();

    
    $primerN = $_POST['primerNombre'];
    $primerNomb = strtolower($primerN);
    $primerNombre = ucfirst($primerNomb);

    $segundoN = $_POST['segundoNombre'];
    $segundoNom = strtolower($segundoN);
    $segundoNombre = ucfirst($segundoNom);

    $primerA = $_POST['primerApellido'];
    $primerApell = strtolower($primerA);
    $primerApellido = ucfirst($primerApell);

    $segundoA = $_POST['segundoApellido'];
    $segundoApell = strtolower($segundoA);
    $segundoApellido = ucfirst($segundoApell);

    $foto = $_FILES['file']['name'];
    $ruta = $_FILES['file']['tmp_name'];
    $destino = "../img/fotos/".$foto;
    copy($ruta,$destino);

    
 
    $exitoRegistro = $objetoPersona->actualizarPersonalTesis($_REQUEST['idPersona'],
                                                    $_REQUEST['idRol'],
                                                    $_REQUEST['ci'],
                                                    $primerNombre,
                                                    $segundoNombre,
                                                    $primerApellido,
                                                    $segundoApellido,
                                                    $destino
    );
    
        //  exit();                      

    if($exitoRegistro==1){
        ?>
        <script>
            alert("Exito al actualizar");
            window.location = "../View/IUListaPersonalTesisAdmi.php";
        </script>

<?php

    }else
    {
        ?>
        <script>
            alert("Error al actualizar");
            window.location = "../View/IUListaPersonalTesisAdmi.php";
        </script>

<?php
 
     }
        
        
    
?>