<?php
    class Rol
    {
        public $idRol;
        public $nombre;


        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        
        public function setIdRol($idRol){$this->idRol = $idRol;}
        public function setNombre($nombre){$this->nombre = $nombre;}
        
 
        public function getIdRol(){return $this->idRol;}
        public function getNombre(){return $this->nombre;}
       

        public function listaRol()
        {
            
            $sqlListaRol = "SELECT *
            FROM rol
            ORDER BY nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaRol);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaRolDeLaConsulta = $cmd->fetchAll();
    
            return $listaRolDeLaConsulta;
    
        }//end function


    }



?>
