<?php
    class RolPersonalTesis
    {
        public $idRolPersonalTesis;
        public $nombre;


        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        
        public function setIdRolPersonalTesis($idRolPersonalTesis){$this->idRolPersonalTesis = $idRolPersonalTesis;}
        public function setNombre($nombre){$this->nombre = $nombre;}
        
 
        public function getIdRolPersonalTesis(){return $this->idRolPersonalTesis;}
        public function getNombre(){return $this->nombre;}
       

        public function listaRolPersonalTesis()
        {
            
            $sqlListaRolPersonalTesis = "SELECT *
            FROM rolPersonalTesis
            ORDER BY nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaRolPersonalTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaRolPersonalTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaRolPersonalTesisDeLaConsulta;
    
        }//end function


    }



?>
