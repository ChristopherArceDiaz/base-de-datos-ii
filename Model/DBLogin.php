<?php


require_once("../Model/Conexion.php");

    class DBLogin
    {
        private $conexion;
        function __construct()
        {
            $this->conexion = new Conexion();
        }

        public function ingresoLoginModelo($datos)
        {
            $sql = "SELECT * FROM persona WHERE usuario = :usuario";
            
            $cmd = $this->conexion->prepare($sql);
            //asignando los valores de los parametros
            $cmd->bindParam(':usuario', $datos['usuario']);

            $cmd->execute();

            return $cmd->fetch();
        }
    }


?>