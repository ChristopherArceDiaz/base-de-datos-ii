<?php

    class PersonalTesis
    {
        public $idPersonalTesis;
        // public $nombre;
        public $ci;
        public $primerNombre;
        public $segundoNombre;
        public $apellidoPaterno;
        public $apellidoMaterno;
       

        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        public function setIdPersonalTesis($idPersonalTesis){$this->idPersonalTesis = $idPersonalTesis;}
        // public function setNombre($nombre){$this->nombre = $nombre;}
        public function setCi($ci){$this->ci = $ci;}
        public function setPrimerNombre($primerNombre){$this->primerNombre = $primerNombre;}
        public function setSegundoNombre($segundoNombre){$this->segundoNombre = $segundoNombre;}
        public function setApellidoPaterno($apellidoPaterno){$this->apellidoPaterno = $apellidoPaterno;}
        public function setApellidoMaterno($apellidoMaterno){$this->apellidoMaterno = $apellidoMaterno;}


        public function getIdPersonalTesis(){return $this->idPersonalTesis;}
        // public function getNombre(){return $this->nombre;}
        public function getCi(){return $this->ci;}
        public function getPrimerNombre(){return $this->primerNombre;}
        public function getSegundoNombre(){return $this->segundoNombre;}
        public function getApellidoPaterno(){return $this->apellidoPaterno;}
        public function getApellidoMaterno(){return $this->apellidoMaterno;}


        //METODO SEGURO

        public function listaPersonalTesis()
        {
            $sqlListaDePersonasTesis = "SELECT p.idPersonalTesis AS idPersonalTesis, CONCAT_WS(' ', p.apellidoPaterno, p.apellidoMaterno, p.primerNombre, p.segundoNombre) AS PersonalTesis, rp.nombre AS nombre
            FROM personaltesis p INNER JOIN rolpersonaltesis rp
            WHERE p.idRolPersonalTesis = 1
             ORDER BY p.apellidoPaterno";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonasTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasTesisDeLaConsulta;
    
        }//end function

        public function personalTesisTutor()
        {
            $sqlListaDePersonasTesis = "SELECT idPersonalTesis AS idPersonalTesis, CONCAT_WS(' ', apellidoPaterno, apellidoMaterno,primerNombre, segundoNombre) AS Personal
            FROM personaltesis
            WHERE idRolPersonalTesis = 1
             ORDER BY apellidoPaterno";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonasTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasTesisDeLaConsulta;
    
        }//end function

        public function personalTesisRevisor()
        {
            $sqlListaDePersonasTesis = "SELECT idPersonalTesis AS idPersonalTesis, CONCAT_WS(' ', apellidoPaterno, apellidoMaterno,primerNombre, segundoNombre) AS Personal
            FROM personaltesis
            WHERE idRolPersonalTesis = 2
             ORDER BY apellidoPaterno";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonasTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasTesisDeLaConsulta;
    
        }//end function

        public function listaDePersonalTesis()
        {
            //HASTA ROL, MUESTRA LOS 153 USUARIOS, ASIGNACION MATERIA YA NO MUESTRA
            //SELECT CONCAT_WS(' ',p.primerApellido, p.seg  undoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS rol
            //from persona p INNER JOIN rol r ON p.idRol = r.idRol
            //INNER JOIN asignacionCarrera am ON p.idPersona = am.idPersona
            //INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera

            $sqlListaDePersonas = "SELECT p.idPersona AS idPersona, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, fac.nombre AS Facultad, ca.nombre AS Carrera, p.fechaRegistro AS fechaRegistro
            FROM facultad fac INNER JOIN carrera ca ON fac.idFacultad = ca.idFacultad
            INNER JOIN asignacionCarrera am ON ca.idCarrera = am.idCarrera
            INNER JOIN persona p ON p.idPersona = am.idPersona
            INNER JOIN rol r ON p.idRol = r.idRol
            ORDER BY p.primerApellido;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasDeLaConsulta;
    
        }//end function


       public function registrarTutor($idDocumentoTesis,$idPersonalTesis) 
        {  
            $sqlInsertarTutor = "INSERT INTO participantesTesis(idDocumentoTesis,idPersonalTesis)
            VALUES (:idDocumentoTesis,:idPersonalTesis)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarTutor);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idDocumentoTesis', $idDocumentoTesis);
                    $cmd->bindParam(':idPersonalTesis', $idPersonalTesis);

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function

        
       public function registrarRevisor1($idDocumentoTesis,$idPersonalTesis) 
       {  
           $sqlInsertarRevisor1 = "INSERT INTO participantesTesis(idDocumentoTesis,idPersonalTesis)
           VALUES (:idDocumentoTesis,:idPersonalTesis)";
           try{
                   $cmd = $this->conexion->prepare($sqlInsertarRevisor1);
                   //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                   $cmd->bindParam(':idDocumentoTesis', $idDocumentoTesis);
                   $cmd->bindParam(':idPersonalTesis', $idPersonalTesis);

                   $cmd->execute();

                   $registroAfectado = $cmd->rowCount();
                   if($registroAfectado>0){
                       return 1;    
                   }else{
                       return 0;
                   }

           }catch(PDOException $e){
               echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
               exit();
               return 0;
           }
       }//end function




       public function registrarRevisor2($idDocumentoTesis,$idPersonalTesis) 
       {  
           $sqlInsertarRevisor2 = "INSERT INTO participantesTesis(idDocumentoTesis,idPersonalTesis)
           VALUES (:idDocumentoTesis,:idPersonalTesis)";
           try{
                   $cmd = $this->conexion->prepare($sqlInsertarRevisor2);
                   //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                   $cmd->bindParam(':idDocumentoTesis', $idDocumentoTesis);
                   $cmd->bindParam(':idPersonalTesis', $idPersonalTesis);

                   $cmd->execute();

                   $registroAfectado = $cmd->rowCount();
                   if($registroAfectado>0){
                       return 1;    
                   }else{
                       return 0;
                   }

           }catch(PDOException $e){
               echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
               exit();
               return 0;
           }
       }//end function


        public function registrarAutor1($idDocumentoTesis,$idAsignacionCarrera) 
        {  
            $sqlInsertarPersona = "INSERT INTO autor(idDocumentoTesis,idAsignacionCarrera)
            VALUES (:idDocumentoTesis,:idAsignacionCarrera)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarPersona);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idDocumentoTesis', $idDocumentoTesis);
                    $cmd->bindParam(':idAsignacionCarrera', $idAsignacionCarrera);

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function registrarAutor2($idDocumentoTesis,$idAsignacionCarrera) 
        {  
            $sqlInsertarAutor = "INSERT INTO autor(idDocumentoTesis,idAsignacionCarrera)
            VALUES (:idDocumentoTesis,:idAsignacionCarrera)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarAutor);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idDocumentoTesis', $idDocumentoTesis);
                    $cmd->bindParam(':idAsignacionCarrera', $idAsignacionCarrera);

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function





    }

?>

