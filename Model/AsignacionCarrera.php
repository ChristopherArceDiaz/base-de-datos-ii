<?php
    class AsignacionCarrera
    {
        public $idAsignacionCarrera;
        public $idCarrera;
        public $idPersona;


        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        
        public function setIdAsignacionCarrera($idAsignacionCarrera){$this->idAsignacionCarrera = $idAsignacionCarrera;}
        public function setIdCarrera($idCarrera){$this->idCarrera = $idCarrera;}
        public function setIdPersona($idPersona){$this->idPersona = $idPersona;}
 
        public function getIdAsignacionCarrera(){return $this->idAsignacionCarrera;}
        public function getIdCarrera(){return $this->idCarrera;}
        public function getIdPersona(){return $this->idPersona;}
       

        // public function listaAsignacionCarrera()
        // {
           
        //     $sqlListaAsignacionCarrera = "SELECT *
        //     FROM asignacioncarrera
        //     ORDER BY idAsignacionCarrera DESC;";

            
        //     //preparando para ejecutar la consulta.
        //     $cmd = $this->conexion->prepare($sqlListaAsignacionCarrera);
        //     //ejecuta la consulta
        //     $cmd->execute();
        //     //variable para recibir la consulta en un areglo
        //     $listaAsignacionCarreraDeLaConsulta = $cmd->fetchAll();
    
        //     return $listaAsignacionCarreraDeLaConsulta;
    
        // }//end function

        public function listaAsignacionCarrera()
        {
            $sqlListaDocumentoTesis = "SELECT DISTINCT am.idAsignacionCarrera AS idAsignacionCarrera, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor
            FROM persona p INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            AND p.idRol = '3'
            ORDER BY p.primerApellido";
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDocumentoTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDocumentoTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDocumentoTesisDeLaConsulta;
    
        }//end function


        public function registrarAsignacionCarrera($idCarrera,$idPersona) //$idAsignacionCarrera
        {   
            //idAsignacionCarrera
            // :idAsignacionCarrera
            $sqlInsertarAsignacionCarrera = "
            INSERT INTO asignacionCarrera(idCarrera,idPersona) 
            VALUES (:idCarrera,:idPersona);  
                                  ";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarAsignacionCarrera);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    // $cmd->bindParam(':idAsignacionCarrera', $idAsignacionCarrera);
                   
                    $cmd->bindParam(':idCarrera', $idCarrera);
                    $cmd->bindParam(':idPersona', $idPersona);
                
                    $cmd->execute();
                    
                    // return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        // return 1;
                        // echo "ID ultimo: ".$this->conexion->lastInsertId();
                        // header('Location: ../View/IURegistrarTesis.php?idUltimo='.$this->conexion->lastInsertId());


                        header('location: ../View/IURegistrarTesis.php?idCarrera='.$idCarrera.'&idUltimo='.$this->conexion->lastInsertId());
                        // $_SESSION['idUltimo'] = $cmd->lastInsertId();  
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function




        public function registrarAsignacionCarrera2($idCarrera,$idPersona) //$idAsignacionCarrera
        {   
            //idAsignacionCarrera
            // :idAsignacionCarrera
            $sqlInsertarAsignacionCarrera = "
            INSERT INTO asignacionCarrera(idCarrera,idPersona) 
            VALUES (:idCarrera,:idPersona);  
                                  ";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarAsignacionCarrera);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    // $cmd->bindParam(':idAsignacionCarrera', $idAsignacionCarrera);
                   
                    $cmd->bindParam(':idCarrera', $idCarrera);
                    $cmd->bindParam(':idPersona', $idPersona);
                
                    $cmd->execute();
                    
                    // return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        return 1;
                        // echo "ID ultimo: ".$this->conexion->lastInsertId();
                        // header('Location: ../View/IURegistrarTesis.php?idUltimo='.$this->conexion->lastInsertId());


                        // header('location: ../View/IURegistrarTesis.php?idCarrera='.$idCarrera.'&idUltimo='.$this->conexion->lastInsertId());
                        // $_SESSION['idUltimo'] = $cmd->lastInsertId();  
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function
    }



?>
