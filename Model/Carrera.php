<?php
    class Carrera
    {
        public $idCarrera;
        public $idUniversidad;
        public $nombre;
        public $sigla;
        

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        
        public function setIdCarrera($idCarrera){$this->idCarrera = $idCarrera;}
        public function setIdUniversidad($idUniversidad){$this->idUniversidad = $idUniversidad;}
        public function setNombre($nombre){$this->nombre = $nombre;}
        public function setSigla($sigla){$this->sigla = $sigla;}
        
        
 
        public function getIdCarrera(){return $this->idCarrera;}
        public function getIdUniversidad(){return $this->idUniversidad;}
        public function getNombre(){return $this->nombre;}
        public function getSigla(){return $this->sigla;}
        
       

        public function listaCarrera()
        {
            
            $sqlListaCarrera = "SELECT *
            FROM carrera
            ORDER BY nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaCarrera);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $listaCarreraDeLaConsulta;
    
        }//end function

        public function listaCarreraRegistro($idFacultad)
        {
            
            $sqlListaCarreraRegistro = "SELECT *
            FROM carrera
            WHERE idFacultad = :idFacultad
            ORDER BY nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaCarreraRegistro);
            $cmd->bindParam(':idFacultad', $idFacultad);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaCarreraRegistroDeLaConsulta = $cmd->fetchAll();
    
            return $listaCarreraRegistroDeLaConsulta;
    
        }//end function


        public function listaCantidadTeisCarrera()
        {
            $sqlListaCantidadTeisCarrera = "SELECT ca.nombre AS Carrera, COUNT(dt.idDocumentoTesis) AS Cantidad, ca.sigla AS Sigla
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            GROUP BY ca.nombre
            ORDER BY ca.nombre;
            ";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaCantidadTeisCarrera);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaCantidadTeisCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $listaCantidadTeisCarreraDeLaConsulta;
    
        }//end function


        public function listaCantidadModalidadTeisCarrera($idCarrera)
        {
            $sqlListaCantidadModalidadTeisCarrera = "SELECT fac.nombre AS Facultad, ca.nombre AS Carrera, COUNT(dt.idDocumentoTesis) AS Cantidad, tT.nombre AS TipoTesis
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND ca.idCarrera = :idCarrera
            AND r.idRol = '3'
            GROUP BY tT.nombre
            ORDER BY dt.fechaHoraRegistro;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaCantidadModalidadTeisCarrera);

            $cmd->bindParam(':idCarrera', $idCarrera);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaCantidadModalidadTeisCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $listaCantidadModalidadTeisCarreraDeLaConsulta;
    
        }//end function

        public function listaDocumentoTesisID($idCarrera)
        {
        
            $sqlListaDocumentoTesisID = "SELECT ca.nombre AS Carrera, dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND ca.idCarrera = :idCarrera
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDocumentoTesisID);

            $cmd->bindParam(':idCarrera', $idCarrera);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDocumentoIDTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDocumentoIDTesisDeLaConsulta;
    
        }//end function

        
    }




?>

