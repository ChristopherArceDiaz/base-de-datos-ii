<?php
	/**
	* Clase de conexion a la DB
	*/
	class Conexion extends PDO
	{
		private $tipoDB = 'mysql';
		private $servidor = 'localhost';
		private $user = 'root';
		private $password = '';
		private $db = 'tesis2';

		public function __construct()
		{
			try {
				  parent::__construct($this->tipoDB.':host='.$this->servidor.';dbname='.$this->db, $this->user, $this->password);	
				  //echo "Conexion exitosa"." maria";
			} catch (PDOException $e ) {
				echo 'ERROR: No se logro hacer una conexion a la Base de Datos - '.$e->getMessage();
				exit;
			}
		}
	}
		$connect = new Conexion();

		//CONEXION A LA BASE DE DATOS FORMA 2
		/* Conectar a una base de datos de MySQL invocando al controlador */
		// $dsn = 'mysql:dbname=tarea1;host=127.0.0.1';
		// $usuario = 'root';
		// $contraseña = '';

		// try {
		// 	$gbd = new PDO($dsn, $usuario, $contraseña);
		// } catch (PDOException $e) {
		// 	echo 'Falló la conexión: ' . $e->getMessage();
		// }
			


?>