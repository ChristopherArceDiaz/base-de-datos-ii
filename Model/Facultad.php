<?php
    class Facultad
    {
        public $idFacultad;
        public $idUniversidad;
        public $nombre;
        public $sigla;
        


        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        
        public function setIdFacultad($idFacultad){$this->idFacultad = $idFacultad;}
        public function setIdUniversidad($idUniversidad){$this->idUniversidad = $idUniversidad;}
        public function setNombre($nombre){$this->nombre = $nombre;}
        public function setSigla($sigla){$this->sigla = $sigla;}
        
        
 
        public function getIdFacultad(){return $this->idFacultad;}
        public function getIdUniversidad(){return $this->idUniversidad;}
        public function getNombre(){return $this->nombre;}
        public function getSigla(){return $this->sigla;}
        
       

        public function listaFacultad()
        {
           
            $sqlListaFacultad = "SELECT *
            FROM facultad
            ORDER BY nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaFacultad);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaFacultadDeLaConsulta = $cmd->fetchAll();
    
            return $listaFacultadDeLaConsulta;
    
        }//end function


        public function listaCantidadTeisFacultad()
        {
            // $sqlListaDocumentoTesis = "SELECT * FROM usuario";
            // $sqlListaDocumentoTesis = "SELECT ci,apellidoPaterno,apellidoMaterno,primerNombre,segundoNombre,usuario,contrasenia FROM usuario";
            // $sqlListaDocumentoTesis = "SELECT ci,CONCAT_WS(' ', apellidoPaterno,apellidoMaterno,primerNombre,segundoNombre) as nombreCompleto,usuario,contrasenia FROM usuario";
            $sqlListaCantidadTeisFacultad = "SELECT fac.idFacultad AS idFacultad, fac.nombre AS Nombre, COUNT(dt.idDocumentoTesis) AS Cantidad, fac.sigla AS Sigla
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            GROUP BY fac.nombre
            ORDER BY fac.nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaCantidadTeisFacultad);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaCantidadTeisFacultadDeLaConsulta = $cmd->fetchAll();
    
            return $listaCantidadTeisFacultadDeLaConsulta;
    
        }//end function

        public function listaCantidadTeisFacultadGrafico()
        {
            $arreglo = array();

            $sqlListaCantidadTeisFacultadGrafico = "SELECT fac.idFacultad AS idFacultad, fac.nombre AS Nombre, COUNT(dt.idDocumentoTesis) AS Cantidad, fac.sigla AS Sigla
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            GROUP BY fac.nombre
            ORDER BY fac.nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaCantidadTeisFacultadGrafico);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaCantidadTeisFacultadGraficoDeLaConsulta = $cmd->fetchAll();
            
            while ($graficos = fetchAll($sqlListaCantidadTeisFacultadGrafico)) {
                $arreglo[] = $graficos;
            }

            // return $listaCantidadTeisFacultadGraficoDeLaConsulta;
            return $arreglo;
    
        }//end function

  


        
        public function porCarreraCuantasTesis($idFacultad)
        {
            
            $sqlPorCarreraCuantasTesis = "SELECT fac.idFacultad AS idFacultad, fac.nombre AS Facultad, ca.nombre AS Carrera,COUNT(dt.codigoTesis) AS Cantidad, ca.idCarrera AS idCarrera
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            AND fac.idFacultad = :idFacultad
            GROUP BY ca.nombre
            ORDER BY ca.nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlPorCarreraCuantasTesis);

            $cmd->bindParam(':idFacultad', $idFacultad);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $porCarreraCuantasTesisDeLaConsulta = $cmd->fetchAll();
    
            return $porCarreraCuantasTesisDeLaConsulta;
    
        }//end function



         
        public function porAnioCantidadTesis($idFacultad)
        {
            
            $sqlPorAnioCantidadTesis = " SELECT YEAR(dt.fechaHoraRegistro) Anio,fac.idFacultad AS idFacultad, dt.fechaHoraRegistro AS Fecha,fac.nombre AS Facultad, COUNT(dt.fechaHoraRegistro) AS Cantidad, ca.idCarrera AS idCarrera
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            AND fac.idFacultad = :idFacultad            
            GROUP BY Anio
            ORDER BY Anio DESC;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlPorAnioCantidadTesis);

            $cmd->bindParam(':idFacultad', $idFacultad);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $porAnioCantidadTesisDeLaConsulta = $cmd->fetchAll();
    
            return $porAnioCantidadTesisDeLaConsulta;
    
        }//end function

    }



?>
