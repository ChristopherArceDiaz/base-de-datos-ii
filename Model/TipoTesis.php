<?php
    class TipoTesis
    {
        public $idTipoTesis;
        public $nombre;


        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        
        public function setIdTipoTesis($idTipoTesis){$this->idTipoTesis = $idTipoTesis;}
        public function setNombre($nombre){$this->nombre = $nombre;}
        
 
        public function getIdTipoTesis(){return $this->idTipoTesis;}
        public function getNombre(){return $this->nombre;}
       

        public function listaTipoTesis()
        {
            
            $sqlListaTipoTesis = "SELECT *
            FROM tipoTesis
            ORDER BY nombre;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaTipoTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaTipoTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaTipoTesisDeLaConsulta;
    
        }//end function


    }



?>
