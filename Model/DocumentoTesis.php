<?php

    class DocumentoTesis
    {
        public $idDocumentoTesis;
        public $idAsignacionCarrera;
        public $idTipoTesis;
        public $codigoTesis;
        public $fechaHoraRegistro;
        public $titulo;
        public $resumen;
        public $introduccion;
        public $palabrasClave;
        public $imagenTapaTesis;
        public $documentoCompleto;


        public $fecha1;
        public $fecha2;

        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        public function setIdDocumentoTesis($idDocumentoTesis){$this->idDocumentoTesis = $idDocumentoTesis;}
        public function setIdAsignacionCarrera($idAsignacionCarrera){$this->idAsignacionCarrera = $idAsignacionCarrera;}
        public function setIdTipoTesis($idTipoTesis){$this->idTipoTesis = $idDocumentoTesis;}
        public function setCodigoTesis($codigoTesis){$this->codigoTesis = $codigoTesis;}
        public function setFechaHoraRegistro($fechaHoraRegistro){$this->fechaHoraRegistro = $fechaHoraRegistro;}
        public function setTitulo($titulo){$this->titulo = $titulo;}
        public function setResumen($resumen){$this->resumen = $resumen;}
        public function setIntroduccion($introduccion){$this->introduccion = $introduccion;}
        public function setPalabrasClave($palabrasClave){$this->palabrasClave = $palabrasClave;}
        public function setImagenTapaTesis($imagenTapaTesis){$this->imagenTapaTesis = $imagenTapaTesis;}
        public function setDocumentoCompleto($documentoCompleto){$this->documentoCompleto = $documentoCompleto;}


        public function setFecha1($fecha1){$this->fecha1 = $fecha1;}
        public function setFecha2($fecha2){$this->fecha2 = $fecha2;}
 


        public function getIdDocumentoTesis(){return $this->idDocumentoTesis;}
        public function getIdAsignacionCarrera(){return $this->idAsignacionCarrera;}
        public function getIdTipoTesis(){return $this->idTipoTesis;}
        public function getCodigoTesis(){return $this->codigoTesis;}
        public function getFechaHoraRegistro(){return $this->fechaHoraRegistro;}
        public function getTitulo(){return $this->titulo;}
        public function getResumen(){return $this->resumen;}
        public function getIntroduccion(){return $this->introduccion;}
        public function getPalabrasClave(){return $this->palabrasClave;}
        public function getImagenTapaTesis(){return $this->imagenTapaTesis;}
        public function getDocumentoCompleto(){return $this->documentoCompleto;}

        public function getFecha1(){return $this->fecha1;}
        public function getFecha2(){return $this->fecha2;}


        public function listaTesis()
        {
            // $sqlListaDocumentoTesis = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            // FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            // INNER JOIN participantesTesis pt ON p.idPersona = pt.idPersona
            // INNER JOIN documentoTesis dt ON pt.idDocumentoTesis = dt.idDocumentoTesis
            // INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            // AND r.idRol = '3'
            // ORDER BY dt.fechaHoraRegistro DESC";

            $listaTesis = "SELECT * FROM documentotesis 
           
            ORDER BY fechaHoraRegistro DESC";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($listaTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaTesisDeLaConsulta;
    
        }//end function
   


        public function listaDocumentoTesis()
        {
            // $sqlListaDocumentoTesis = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            // FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            // INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            // INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            // INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            // INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            // INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            // INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            // INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            // INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            // AND r.idRol = '3'
            // ORDER BY dt.fechaHoraRegistro DESC";

            
            $sqlListaDocumentoTesis = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, GROUP_CONCAT(DISTINCT ' ', p.primerApellido, ' ', p.segundoApellido, ' ', p.primerNombre, ' ', p.segundoNombre) AS Autor, GROUP_CONCAT(' ', pert.apellidoPaterno, ' ', pert.apellidoMaterno, ' ', pert.primerNombre, ' ', pert.segundoNombre) AS Tutor,dt.titulo AS Titulo, tT.nombre AS Nombre
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            GROUP BY dt.idDocumentoTesis
            ORDER BY dt.fechaHoraRegistro DESC";


            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDocumentoTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDocumentoTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDocumentoTesisDeLaConsulta;
    
        }//end function

        public function listaTutor()
        {
            $sqlListaDocumentoTesis = "SELECT DISTINCT CONCAT_WS(' ', p.apellidoPaterno, p.apellidoMaterno,p.primerNombre, p.segundoNombre) AS Tutor, r.nombre AS Nombre
            FROM personalTesis p INNER JOIN rolPersonalTesis r ON p.idRolPersonalTesis = r.idRolPersonalTesis
            AND p.idRolPersonalTesis = '1'
            ORDER BY p.apellidoPaterno;";

            // $sqlListaDocumentoTesis = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis, dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            // FROM documentoTesis dt INNER JOIN participantesTesis pt ON pt.idDocumentoTesis = dt.idDocumentoTesis
            // INNER JOIN persona p ON p.idPersona = pt.idPersona
            // INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            // ORDER BY dt.fechaHoraRegistro DESC";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDocumentoTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDocumentoTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDocumentoTesisDeLaConsulta;
    
        }//end function

        public function listaRevisor()
        {
            $sqlListaDocumentoTesis = "SELECT DISTINCT CONCAT_WS(' ', p.apellidoPaterno, p.apellidoMaterno,p.primerNombre, p.segundoNombre) AS Tutor, r.nombre AS Nombre
            FROM personalTesis p INNER JOIN rolPersonalTesis r ON p.idRolPersonalTesis = r.idRolPersonalTesis
            AND p.idRolPersonalTesis = '2'
            ORDER BY p.apellidoPaterno;";

            // $sqlListaDocumentoTesis = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis, dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            // FROM documentoTesis dt INNER JOIN participantesTesis pt ON pt.idDocumentoTesis = dt.idDocumentoTesis
            // INNER JOIN persona p ON p.idPersona = pt.idPersona
            // INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            // ORDER BY dt.fechaHoraRegistro DESC";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDocumentoTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDocumentoTesisDeLaConsulta = $cmd->fetchAll();
    
            return $listaDocumentoTesisDeLaConsulta;
    
        }//end function

        public function listaAnioTesis()
        {
            $sqlListaAnioTesis = "SELECT DISTINCT YEAR(fechaHoraRegistro) Anio
            FROM documentoTesis
            ORDER BY fechaHoraRegistro DESC";

            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaAnioTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            return $cmd->fetchAll();
    
            // return $listaDocumentoTesisDeLaConsulta;
    
        }//end function

        public function informacionDocumentoTesisTitulo($idTituloDocumento)
        {
            // $sqlInformacionDocumentoTesisTitulo = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, YEAR(dt.fechaHoraRegistro) AS Anio, dt.fechaHoraRegistro AS Fecha, tT.nombre AS TipoBibliografia, fac.nombre AS Facultad, ca.nombre AS Carrera, dt.resumen AS Resumen, dt.codigoTesis AS Codigo, dt.imagenTapaTesis AS TapaTesis, dt.introduccion AS Introduccion, dt.palabrasClave AS PalabrasClave, dt.documentoCompleto AS DocumentoCompleto, p.fotografia AS fotografia, pert.fotografia AS foto
            // FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            // INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            // INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            // INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            // INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            // INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            // INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            // INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            // INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            // AND r.idRol = '3'
            // AND dt.idDocumentoTesis = :idTituloDocumento
            // ORDER BY fac.nombre DESC;";

            $sqlInformacionDocumentoTesisTitulo = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis, GROUP_CONCAT(DISTINCT ' ', p.primerApellido, ' ', p.segundoApellido, ' ', p.primerNombre, ' ', p.segundoNombre) AS Autor, dt.titulo AS Titulo, YEAR(dt.fechaHoraRegistro) AS Anio, dt.fechaHoraRegistro AS Fecha, tT.nombre AS TipoBibliografia, fac.nombre AS Facultad, ca.nombre AS Carrera, dt.resumen AS Resumen, dt.codigoTesis AS Codigo, dt.imagenTapaTesis AS TapaTesis, dt.introduccion AS Introduccion, dt.palabrasClave AS PalabrasClave, dt.documentoCompleto AS DocumentoCompleto, p.fotografia AS fotografia, pert.fotografia AS foto
          FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            AND dt.idDocumentoTesis = :idTituloDocumento
            ORDER BY fac.nombre DESC;";

 



            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlInformacionDocumentoTesisTitulo);

            $cmd->bindParam(':idTituloDocumento', $idTituloDocumento);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $informacionDocumentoTesisTituloDeLaConsulta = $cmd->fetchAll();
    
            return $informacionDocumentoTesisTituloDeLaConsulta;
    
        }//end function



        public function informacionTutor($idTituloDocumento)
        {

            $sqlInformacionDocumentoTesisTitulo = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis, GROUP_CONCAT(DISTINCT ' ', pert.apellidoPaterno, ' ', pert.apellidoMaterno, ' ', pert.primerNombre, ' ', pert.segundoNombre) AS Revisor
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            AND pert.idRolPersonalTesis = 1
            AND dt.idDocumentoTesis = :idTituloDocumento
            ORDER BY fac.nombre DESC;";

 



            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlInformacionDocumentoTesisTitulo);

            $cmd->bindParam(':idTituloDocumento', $idTituloDocumento);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $informacionDocumentoTesisTituloDeLaConsulta = $cmd->fetchAll();
    
            return $informacionDocumentoTesisTituloDeLaConsulta;
    
        }//end function


        public function informacionRevisor($idTituloDocumento)
        {

            $sqlInformacionDocumentoTesisTitulo = "SELECT DISTINCT dt.idDocumentoTesis AS idDocumentoTesis, GROUP_CONCAT(DISTINCT ' ', pert.apellidoPaterno, ' ', pert.apellidoMaterno, ' ', pert.primerNombre, ' ', pert.segundoNombre) AS Revisor
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            AND r.idRol = '3'
            AND pert.idRolPersonalTesis = 2
            AND dt.idDocumentoTesis = :idTituloDocumento
            ORDER BY fac.nombre DESC;";

 



            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlInformacionDocumentoTesisTitulo);

            $cmd->bindParam(':idTituloDocumento', $idTituloDocumento);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $informacionDocumentoTesisTituloDeLaConsulta = $cmd->fetchAll();
    
            return $informacionDocumentoTesisTituloDeLaConsulta;
    
        }//end function

        
        public function busquedaPorDosFechas($fecha1,$fecha2)
        {
        

            $sqlBusquedaPorDosFechas = "SELECT YEAR(dt.fechaHoraRegistro) AS Anio,dt.fechaHoraRegistro AS Fecha,dt.titulo AS Titulo,fac.nombre AS Facultad, ca.nombre AS Carrera, COUNT(dt.idDocumentoTesis) AS Cantidad
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE YEAR(dt.fechaHoraRegistro) BETWEEN :fecha1 AND :fecha2
            AND r.idRol = '3'
            GROUP BY fac.nombre
            ORDER BY Anio DESC;";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaPorDosFechas);

            $cmd->bindParam(':fecha1', $fecha1);
            $cmd->bindParam(':fecha2', $fecha2);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaPorDosFechasDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaPorDosFechasDeLaConsulta;
    
        }//end function


   
        
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //BUSQUEDAS

        public function busquedaUsuarioExternoFacultadTipoTesisAnio($facultad,$carrera,$tipotesis,$anio)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT YEAR(dt.fechaHoraRegistro) AS Anio, dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE fac.idFacultad = :facultad
            AND ca.idCarrera = :carrera
            AND dt.idTipoTesis = :tipotesis
            AND YEAR(dt.fechaHoraRegistro) = :anio
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

            $cmd->bindParam(':facultad', $facultad);
            $cmd->bindParam(':carrera', $carrera);
            $cmd->bindParam(':tipotesis', $tipotesis);
            $cmd->bindParam(':anio', $anio);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function

        public function busquedaUsuarioCarreraAnioTipoTesis($carrera,$tipotesis,$anio)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE ca.idCarrera = :carrera
            AND dt.idTipoTesis = :tipotesis
            AND YEAR(dt.fechaHoraRegistro) = :anio
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

            $cmd->bindParam(':carrera', $carrera);
            $cmd->bindParam(':tipotesis', $tipotesis);
            $cmd->bindParam(':anio', $anio);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function

        public function busquedaUsuarioExternoFacultadCarreraTipoTesis($facultad,$carrera,$tipotesis)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE fac.idFacultad = :facultad
            AND ca.idCarrera = :carrera
            AND dt.idTipoTesis = :tipotesis
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

            $cmd->bindParam(':facultad', $facultad);
            $cmd->bindParam(':carrera', $carrera);
            $cmd->bindParam(':tipotesis', $tipotesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function

   

        // BUSQUEDA POR FACULTAD, CARRERA
        public function busquedaUsuarioExternoFacuCarrera($facultad,$carrera)
        {
        

            $sqlBusquedaUsuarioExternoFacuCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE fac.idFacultad = :facultad
            AND ca.idCarrera = :carrera 
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacuCarrera);

            $cmd->bindParam(':facultad', $facultad);
            $cmd->bindParam(':carrera', $carrera);           
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacuCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacuCarreraDeLaConsulta;
    
        }//end function


        public function busquedaUsuarioExternoFacultadTipoTesis($facultad,$tipotesis)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE fac.idFacultad = :facultad
            AND dt.idTipoTesis = :tipotesis
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

            $cmd->bindParam(':facultad', $facultad);
            $cmd->bindParam(':tipotesis', $tipotesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function

        public function busquedaUsuarioExternoFacultadAnio($facultad,$anio)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE fac.idFacultad = :facultad
            AND YEAR(dt.fechaHoraRegistro) = :anio
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

            $cmd->bindParam(':facultad', $facultad);
            $cmd->bindParam(':anio', $anio);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function




        // BUSQUEDA POR FACULTAD
        public function busquedaUsuarioExternoFacultad($facultad)
        {

            $sqlBusquedaUsuarioExternoFacultad = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE fac.idFacultad = :facultad
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultad);

            $cmd->bindParam(':facultad', $facultad);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadDeLaConsulta;
    
        }//end function


        public function busquedaUsuarioExternoCarreraTipoTesis($carrera,$tipotesis)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE ca.idCarrera = :carrera
            AND dt.idTipoTesis = :tipotesis
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

            $cmd->bindParam(':carrera', $carrera);
            $cmd->bindParam(':tipotesis', $tipotesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function

        public function busquedaUsuarioCarreraBusqueda($carrera,$busqueda)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE ca.idCarrera = :carrera
            AND CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) LIKE '%".$busqueda."%'
              OR p.primerApellido LIKE '%".$busqueda."%'
              OR p.segundoApellido LIKE '%".$busqueda."%'
              OR p.primerNombre LIKE '%".$busqueda."%'
              OR p.segundoNombre LIKE '%".$busqueda."%'
              OR dt.titulo LIKE '%".$busqueda."%'
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

            $cmd->bindParam(':carrera', $carrera);
          
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function

        public function busquedaUsuarioGeneralTipoTesis($tipotesis,$busqueda)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE dt.idTipoTesis = :tipotesis
            AND CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) LIKE '%".$busqueda."%'
              OR p.primerApellido LIKE '%".$busqueda."%'
              OR p.segundoApellido LIKE '%".$busqueda."%'
              OR p.primerNombre LIKE '%".$busqueda."%'
              OR p.segundoNombre LIKE '%".$busqueda."%'
              OR dt.titulo LIKE '%".$busqueda."%'
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

           
            $cmd->bindParam(':tipotesis', $tipotesis);
            // $cmd->bindParam(':anio', $anio);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function


        public function busquedaUsuarioExternoAnioTipoTesis($tipotesis,$anio)
        {
        

            $sqlBusquedaUsuarioExternoFacultadCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE dt.idTipoTesis = :tipotesis
            AND YEAR(dt.fechaHoraRegistro) = :anio
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoFacultadCarrera);

           
            $cmd->bindParam(':tipotesis', $tipotesis);
            $cmd->bindParam(':anio', $anio);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoFacultadCarreraDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoFacultadCarreraDeLaConsulta;
    
        }//end function

                // BUSQUEDA POR FACULTAD
                public function busquedaUsuarioExternoCarrera($carrera)
                {
        
                    $sqlBusquedaUsuarioExternoCarrera = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
                    FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
                    INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
                    INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
                    INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
                    INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
                    INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
                    INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
                    INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
                    INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
                    WHERE ca.idCarrera = :carrera
                    AND r.idRol = '3'
                    ORDER BY dt.fechaHoraRegistro DESC";
                    //preparando para ejecutar la consulta.
                    $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoCarrera);
        
                    $cmd->bindParam(':carrera', $carrera);
                    //ejecuta la consulta
                    $cmd->execute();
                    //variable para recibir la consulta en un areglo
                    return $cmd->fetchAll();
               
            
                }//end function

        // BUSQUEDA POR TIPO DE TESIS
        public function busquedaUsuarioExternoTesis($idTipoTesis)
        {

            $sqlBusquedaUsuarioExternoTesis = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
            WHERE dt.idTipoTesis = :idTipoTesis
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaUsuarioExternoTesis);

            $cmd->bindParam(':idTipoTesis', $idTipoTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $busquedaUsuarioExternoTesisDeLaConsulta = $cmd->fetchAll();
    
            return $busquedaUsuarioExternoTesisDeLaConsulta;
    
        }//end function


          // BUSQUEDA POR TIPO DE TESIS
          public function busquedaUsuarioAnio($anio)
          {
  
              $sqlBusquedaUsuarioAnio = "SELECT YEAR(dt.fechaHoraRegistro) AS Anio, dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
              FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
              WHERE YEAR(dt.fechaHoraRegistro) = :anio
              AND r.idRol = '3'
              ORDER BY dt.fechaHoraRegistro DESC";
              //preparando para ejecutar la consulta.
              $cmd = $this->conexion->prepare($sqlBusquedaUsuarioAnio);
  
              $cmd->bindParam(':anio', $anio);
              //ejecuta la consulta
              $cmd->execute();
              //variable para recibir la consulta en un areglo
              return $cmd->fetchAll();
      
            //   return $busquedaUsuarioAnioDeLaConsulta;
      
          }//end function
          
          public function busquedaUsuarioGeneral($busqueda)
          {
  
              $sqlBusquedaUsuarioGeneral = "SELECT dt.idDocumentoTesis AS idDocumentoTesis,dt.codigoTesis AS Codigo, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Autor, dt.titulo AS Titulo, tT.nombre AS Nombre, dt.fechaHoraRegistro AS Fecha
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            INNER JOIN asignacionCarrera am ON am.idPersona = p.idPersona
            INNER JOIN autor a ON a.idAsignacionCarrera = am.idAsignacionCarrera
            INNER JOIN documentoTesis dt ON a.idDocumentoTesis = dt.idDocumentoTesis
            INNER JOIN participantesTesis pt ON dt.idDocumentoTesis = pt.idDocumentoTesis
            INNER JOIN personalTesis pert ON pert.idPersonalTesis = pt.idPersonalTesis
            INNER JOIN tipotesis tT ON dt.idTipoTesis = tT.idTipoTesis
            INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera
            INNER JOIN facultad fac ON fac.idFacultad = ca.idFacultad
              WHERE CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) LIKE '%".$busqueda."%'
              OR p.primerApellido LIKE '%".$busqueda."%'
              OR p.segundoApellido LIKE '%".$busqueda."%'
              OR p.primerNombre LIKE '%".$busqueda."%'
              OR p.segundoNombre LIKE '%".$busqueda."%'
              OR dt.titulo LIKE '%".$busqueda."%'
              AND r.idRol = '3'
              ORDER BY dt.fechaHoraRegistro DESC";
              //preparando para ejecutar la consulta.
              $cmd = $this->conexion->prepare($sqlBusquedaUsuarioGeneral);
  
            //   $cmd->bindParam(':busqueda', $busqueda);
              //ejecuta la consulta
              $cmd->execute();
              //variable para recibir la consulta en un areglo
              return $cmd->fetchAll();
      
            //   return $busquedaUsuarioAnioDeLaConsulta;
      
          }//end function
         
        // FIN BUSQUEDAS
        ////////////////////////////////////////////////////////////////////

        public function registrarTesis($idTipoTesis,$codigoTesis,$fechaHoraRegistro,$titulo,$resumen,$introduccion,$palabrasClave,$imagenTapaTesis,$documentoCompleto) //$idAsignacionCarrera,$idFacultad,$idCarrera,
        {   
            //idAsignacionCarrera,idFacultad,idCarrera,
            // :idAsignacionCarrera,:idFacultad,:idCarrera,
            $sqlInsertarTesis = "
            INSERT INTO documentotesis(idTipoTesis,codigoTesis,fechaHoraRegistro,titulo,resumen,introduccion,palabrasClave,imagenTapaTesis,documentoCompleto) 
            VALUES (:idTipoTesis,:codigoTesis,:fechaHoraRegistro,:titulo,:resumen,:introduccion,:palabrasClave,:imagenTapaTesis,:documentoCompleto);  
                                  ";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarTesis);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                   
                    $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->bindParam(':codigoTesis', $codigoTesis);
                    $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
                    $cmd->bindParam(':titulo', $titulo);
                    $cmd->bindParam(':resumen', $resumen);
                    $cmd->bindParam(':introduccion', $introduccion);
                    $cmd->bindParam(':palabrasClave', $palabrasClave);
                    $cmd->bindParam(':imagenTapaTesis', $imagenTapaTesis);
                    $cmd->bindParam(':documentoCompleto', $documentoCompleto);
                    //$cmd->bindParam(':fechaCreacion', $fechaCreacion); en este caso, agarraras la fecha actual de tu servidor
                    //$cmd->bindParam(':fechaActualizacion', $fechaActualizacion);// no necesitas crear nada si actualizaras.
                    $cmd->execute();
                    // return 1;
                    $registroAfectado = $cmd->rowCount();
                    
                     if($registroAfectado>0)
                    {
                        // echo "ID ultimo: ".$this->conexion->lastInsertId();
                        // header('Location: ../View/IURegistrarParticipantesTesis.php?idUltimo='.$this->conexion->lastInsertId());
                        // header('Location: ../View/IURegistrarAutor.php?idUltimo='.$this->conexion->lastInsertId());
                        header('Location: ../View/IURegistrarParticipantesTesis.php?idUltimo='.$this->conexion->lastInsertId());
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function
 


        public function actualizarTesis($idDocumentoTesis,$idTipoTesis,$titulo,$resumen,$introduccion) //, $documentoCompleto $imagenTapaTesis,$introduccion,$palabrasClave   
        {   
            //,resumen = :resumen,introduccion = :introduccion,palabrasClave = :palabrasClave
            $sqlActualizarTesis = "UPDATE documentotesis SET idTipoTesis = :idTipoTesis,titulo = :titulo, resumen = :resumen, introduccion = :introduccion WHERE idDocumentoTesis = :idDocumentoTesis;";
            try{
                    $cmd = $this->conexion->prepare($sqlActualizarTesis);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idDocumentoTesis', $idDocumentoTesis);
                    $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->bindParam(':titulo', $titulo);
                    $cmd->bindParam(':resumen', $resumen);
                    $cmd->bindParam(':introduccion', $introduccion);
                    // $cmd->bindParam(':imagenTapaTesis', $imagenTapaTesis);
                    // $cmd->bindParam(':documentoCompleto', $documentoCompleto);
                    // $cmd->bindParam(':palabrasClave', $palabrasClave);
                    // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
                    // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->execute();

                    // $ActualizarConsulta = $cmd->fetchAll();
    
                    // return $ActualizarConsulta;
            
                    return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        echo "ID ultimo: ".$this->conexion->lastInsertId();
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


       

        public function verificarTituloTesis($titulo) 
        {  
            $sqlInsertarTesis = "SELECT * FROM documentoTesis WHERE titulo = :titulo";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarTesis);
                    $cmd->bindParam(':titulo', $titulo);
                   

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function

        public function tesisTitulo()
        {
            $sqlTesisTitulo = "SELECT idDocumentoTesis AS idDocumentoTesis, titulo AS Titulo
            FROM documentoTesis 
            ORDER BY titulo;";

            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlTesisTitulo);

            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $TesisTituloDeLaConsulta = $cmd->fetchAll();
    
            return $TesisTituloDeLaConsulta;
    
        }//end function

    }

?>