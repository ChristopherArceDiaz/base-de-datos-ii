<?php

    class Persona
    {
        public $idPersona;
        // public $nombre;
        public $ci;
        public $primerNombre;
        public $segundoNombre;
        public $apellidoPaterno;
        public $apellidoMaterno;
        public $usuario;
        public $contrasenia;

        // public $nombreCompleto;

        function __Construct()
        {
            $this->conexion =  new Conexion();
        }

        public function setIdPersona($idPersona){$this->idPersona = $idPersona;}
        // public function setNombre($nombre){$this->nombre = $nombre;}
        public function setCi($ci){$this->ci = $ci;}
        public function setPrimerNombre($primerNombre){$this->primerNombre = $primerNombre;}
        public function setSegundoNombre($segundoNombre){$this->segundoNombre = $segundoNombre;}
        public function setApellidoPaterno($apellidoPaterno){$this->apellidoPaterno = $apellidoPaterno;}
        public function setApellidoMaterno($apellidoMaterno){$this->apellidoMaterno = $apellidoMaterno;}
        public function setUsuario($usuario){$this->usuario = $usuario;}
        public function setContrasenia($contrasenia){$this->contrasenia = $contrasenia;}

        // public function setNombreCompleto($nombreCompleto){$this->nombreCompleto = $nombreCompleto;}



        public function getIdPersona(){return $this->idPersona;}
        // public function getNombre(){return $this->nombre;}
        public function getCi(){return $this->ci;}
        public function getPrimerNombre(){return $this->primerNombre;}
        public function getSegundoNombre(){return $this->segundoNombre;}
        public function getApellidoPaterno(){return $this->apellidoPaterno;}
        public function getApellidoMaterno(){return $this->apellidoMaterno;}
        public function getUsuario(){return $this->usuario;}
        public function getContrasenia(){return $this->contrasenia;}

        // public function getNombreCompleto(){return $this->nombreCompleto;}


        //METODO SEGURO

        public function verificarPesonaContrasenia($usuario,$contrasenia)
        {
            // $clave = password_verify($contrasenia);
            // $sqlVerificarPersona= "SELECT * FROM persona WHERE usuario = :usuario AND contrasenia = :contrasenia";
            $sqlVerificarPersona= "SELECT * FROM persona WHERE usuario = :usuario AND contrasenia = :contrasenia";

            $cmd = $this->conexion->prepare($sqlVerificarPersona);
            //asignando los valores de los parametros
            $cmd->bindParam(':usuario', $usuario);

            // $encriptado = password_hash($contrasenia, PASSWORD_DEFAULT);
            // $cmd->bindParam(':contrasenia', $encriptado);

            $cmd->bindParam(':contrasenia', $contrasenia);
            //ejecuta la consulta
            $cmd->execute();    
            

            $registroPersona = $cmd->fetch();
            if($registroPersona)
            {
                return $registroPersona;
            }
            else
            {
                return false;
            }
        }


        public function listaDePersonasRegistro()
        {
            //HASTA ROL, MUESTRA LOS 153 USUARIOS, ASIGNACION MATERIA YA NO MUESTRA
            //SELECT CONCAT_WS(' ',p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS rol
            //from persona p INNER JOIN rol r ON p.idRol = r.idRol
            //INNER JOIN asignacionCarrera am ON p.idPersona = am.idPersona
            //INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera

            $sqlListaDePersonas = "SELECT *
            FROM persona 
            ORDER BY primerApellido;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasDeLaConsulta;
    
        }//end function

        public function listaDePersonas()
        {
            //HASTA ROL, MUESTRA LOS 153 USUARIOS, ASIGNACION MATERIA YA NO MUESTRA
            //SELECT CONCAT_WS(' ',p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS rol
            //from persona p INNER JOIN rol r ON p.idRol = r.idRol
            //INNER JOIN asignacionCarrera am ON p.idPersona = am.idPersona
            //INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera

            $sqlListaDePersonas = "SELECT p.idPersona AS idPersona, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, p.telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, p.usuario AS usuario
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            ORDER BY p.primerApellido;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonas);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasDeLaConsulta;
    
        }//end function


        public function listaDePersonalTesis()
        {
            $sqlListaDePersonal = "SELECT p.idPersonalTesis AS idPersonalTesis, CONCAT_WS(' ', p.apellidoPaterno, p.apellidoMaterno,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado
            FROM personalTesis p INNER JOIN rolPersonalTesis r ON p.idRolPersonalTesis = r.idRolPersonalTesis
            ORDER BY p.apellidoPaterno;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonal);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasDeLaConsulta;
    
        }//end function



        public function listaDeRolPersonalTesis()
        {
            $sqlListaDePersonal = "SELECT * FROM rolPersonalTesis ORDER BY nombre;";
            
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaDePersonal);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonasDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonasDeLaConsulta;
    
        }//end function



        public function listaPersona()
        {
            //HASTA ROL, MUESTRA LOS 153 USUARIOS, ASIGNACION MATERIA YA NO MUESTRA
            //SELECT CONCAT_WS(' ',p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS rol
            //from persona p INNER JOIN rol r ON p.idRol = r.idRol
            //INNER JOIN asignacionCarrera am ON p.idPersona = am.idPersona
            //INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera

            $sqlListaPersona = "SELECT p.idPersona AS idPersona, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, p.fechaRegistro AS fechaRegistro
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            ORDER BY p.primerApellido;";
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlListaPersona);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $listaDePersonaDeLaConsulta = $cmd->fetchAll();
    
            return $listaDePersonaDeLaConsulta;
    
        }//end function

        public function busquedaPersonal($busqueda)
        {

            $sqlBusquedaPersonal = "SELECT p.idPersona AS idPersona, CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS Rol, p.telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, p.usuario AS usuario
            FROM persona p INNER JOIN rol r ON p.idRol = r.idRol
            WHERE CONCAT_WS(' ', p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) LIKE '%".$busqueda."%'
            OR p.primerApellido LIKE '%".$busqueda."%'
            OR p.segundoApellido LIKE '%".$busqueda."%'
            OR p.primerNombre LIKE '%".$busqueda."%'
            OR p.segundoNombre LIKE '%".$busqueda."%'
            OR p.ci LIKE '%".$busqueda."%'
            OR r.nombre LIKE '%".$busqueda."%'
            OR p.telefono LIKE '%".$busqueda."%'
            OR p.usuario LIKE '%".$busqueda."%'
            AND r.idRol = '3'
            ORDER BY dt.fechaHoraRegistro DESC";
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlBusquedaPersonal);

          //   $cmd->bindParam(':busqueda', $busqueda);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            return $cmd->fetchAll();
    
          //   return $busquedaUsuarioAnioDeLaConsulta;
    
        }//end function

        public function perfil($idPersona)
        {
            //HASTA ROL, MUESTRA LOS 153 USUARIOS, ASIGNACION MATERIA YA NO MUESTRA
            //SELECT CONCAT_WS(' ',p.primerApellido, p.segundoApellido,p.primerNombre, p.segundoNombre) AS Usuario, p.ci AS ci, r.nombre AS rol
            //from persona p INNER JOIN rol r ON p.idRol = r.idRol
            //INNER JOIN asignacionCarrera am ON p.idPersona = am.idPersona
            //INNER JOIN carrera ca ON ca.idCarrera = am.idCarrera

            $sqlListaPersona = "SELECT CONCAT_WS(' ',primerApellido, segundoApellido,primerNombre, segundoNombre) AS Persona, ci AS ci, telefono AS telefono, IF(0<activo, 'ACTIVO', 'INACTIVO') AS estado, usuario AS usuario, fechaRegistro AS registro, fechaActualizacion AS actualizacion, fotografia AS foto
            FROM persona 
            WHERE idPersona = :idPersona
            ORDER BY primerApellido;";

            $cmd = $this->conexion->prepare($sqlListaPersona);
            //asignando los valores de los parametros


            // $encriptado = password_hash($contrasenia, PASSWORD_DEFAULT);
            // $cmd->bindParam(':contrasenia', $encriptado);

            $cmd->bindParam(':idPersona', $idPersona);
            //ejecuta la consulta
            $cmd->execute();    

            return $cmd->fetchAll();
    
            // return $listaDePersonasDeLaConsulta;
    
        }//end function


        public function informacionPersona($idPersona)
        {
            $sqlInformacionPersona = "SELECT * FROM persona p INNER JOIN rol r ON p.idRol = r.idRol WHERE idPersona = :idPersona";
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlInformacionPersona);

            $cmd->bindParam(':idPersona', $idPersona);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $informacionPersonaDeLaConsulta = $cmd->fetchAll();
    
            return $informacionPersonaDeLaConsulta;
    
        }//end function

        public function informacionPersonalTesis($idPersonalTesis)
        {
            $sqlInformacionPersonal = "SELECT * FROM personalTesis p INNER JOIN rolPersonalTesis r ON p.idRolPersonalTesis = r.idRolPersonalTesis WHERE p.idPersonalTesis = :idPersonalTesis";
            
            //preparando para ejecutar la consulta.
            $cmd = $this->conexion->prepare($sqlInformacionPersonal);

            $cmd->bindParam(':idPersonalTesis', $idPersonalTesis);
            //ejecuta la consulta
            $cmd->execute();
            //variable para recibir la consulta en un areglo
            $informacionPersonaDeLaConsulta = $cmd->fetchAll();
    
            return $informacionPersonaDeLaConsulta;
    
        }//end function

       public function registrarPersona($idRol,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$usuario,$contrasenia,$fechaRegistro,$fechaActualizacion) 
        {  
            $sqlInsertarPersona = "INSERT INTO persona(idRol,ci,primerNombre,segundoNombre,primerApellido,segundoApellido,telefono,fotografia,usuario,contrasenia, fechaRegistro,fechaActualizacion)
            VALUES (:idRol,:ci,:primerNombre,:segundoNombre,:primerApellido,:segundoApellido,:telefono,:fotografia,:usuario,:contrasenia,:fechaRegistro,:fechaActualizacion)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarPersona);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':primerApellido', $primerApellido);
                    $cmd->bindParam(':segundoApellido', $segundoApellido);
                    $cmd->bindParam(':telefono', $telefono);
                    $cmd->bindParam(':fotografia', $fotografia);
                    $cmd->bindParam(':usuario', $usuario);
                    $cmd->bindParam(':contrasenia', $contrasenia);           
                    $cmd->bindParam(':fechaRegistro', $fechaRegistro);
                    $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);    

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        // return 1;
                        // header('location: ../View/IURegistrarAsignacionCarrera.php?idCarrera='.$idCarrera.'&idUltimo='.$this->conexion->lastInsertId());   
                        header('location: ../View/IURegistrarAsignacionCarrera.php?idUltimo='.$this->conexion->lastInsertId());   
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function registrarPersona2($idRol,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$usuario,$contrasenia,$fechaRegistro,$fechaActualizacion) 
        {  
            $sqlInsertarPersona = "INSERT INTO persona(idRol,ci,primerNombre,segundoNombre,primerApellido,segundoApellido,telefono,fotografia,usuario,contrasenia, fechaRegistro,fechaActualizacion)
            VALUES (:idRol,:ci,:primerNombre,:segundoNombre,:primerApellido,:segundoApellido,:telefono,:fotografia,:usuario,:contrasenia,:fechaRegistro,:fechaActualizacion)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarPersona);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':primerApellido', $primerApellido);
                    $cmd->bindParam(':segundoApellido', $segundoApellido);
                    $cmd->bindParam(':telefono', $telefono);
                    $cmd->bindParam(':fotografia', $fotografia);
                    $cmd->bindParam(':usuario', $usuario);
                    $cmd->bindParam(':contrasenia', $contrasenia);           
                    $cmd->bindParam(':fechaRegistro', $fechaRegistro);
                    $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);    

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        // return 1;
                        // header('location: ../View/IURegistrarAsignacionCarrera.php?idCarrera='.$idCarrera.'&idUltimo='.$this->conexion->lastInsertId());   
                        // header('location: ../View/IURegistrarAsignacionCarrera.php?idUltimo='.$this->conexion->lastInsertId());  
                        header('location: ../View/IURegistrarAsignacionCarrera2.php?usuario='.$usuario.'&contrasenia='.$contrasenia.'&idUltimo='.$this->conexion->lastInsertId()); 
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function



        public function registrarPersonalTesis($idRolPersonalTesis,$ci,$primerNombre,$segundoNombre,$apellidoPaterno,$apellidoMaterno,$fotografia) 
        {  
            $sqlInsertarPersona = "INSERT INTO personalTesis (idRolPersonalTesis,ci,primerNombre,segundoNombre,apellidoPaterno,apellidoMaterno,fotografia)
            VALUES (:idRolPersonalTesis,:ci,:primerNombre,:segundoNombre,:apellidoPaterno,:apellidoMaterno,:fotografia)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarPersona);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idRolPersonalTesis', $idRolPersonalTesis);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':apellidoPaterno', $apellidoPaterno);
                    $cmd->bindParam(':apellidoMaterno', $apellidoMaterno);
                    $cmd->bindParam(':fotografia', $fotografia);
                   

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function verificarRegistrarPersona($ci) 
        {  
            $sqlInsertarPersona = "SELECT * FROM persona WHERE ci = :ci";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarPersona);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':ci', $ci);
                   

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function verificarRegistrarPersonalTesis($ci) 
        {  
            $sqlInsertarPersona = "SELECT * FROM personalTesis WHERE ci = :ci";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarPersona);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':ci', $ci);
                   

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function



        public function registrarUsuario($idRol,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$usuario,$contrasenia,$fechaRegistro,$fechaActualizacion) 
        {  
            $sqlInsertarUsuario = "INSERT INTO persona(idRol,ci,primerNombre,segundoNombre,primerApellido,segundoApellido,telefono,fotografia,usuario,contrasenia, fechaRegistro,fechaActualizacion)
            VALUES (:idRol,:ci,:primerNombre,:segundoNombre,:primerApellido,:segundoApellido,:telefono,:fotografia,:usuario,:contrasenia,:fechaRegistro,:fechaActualizacion)";
            try{
                    $cmd = $this->conexion->prepare($sqlInsertarUsuario);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':primerApellido', $primerApellido);
                    $cmd->bindParam(':segundoApellido', $segundoApellido);
                    $cmd->bindParam(':telefono', $telefono);
                    $cmd->bindParam(':fotografia', $fotografia);
                    $cmd->bindParam(':usuario', $usuario);
                    $cmd->bindParam(':contrasenia', $contrasenia);           
                    $cmd->bindParam(':fechaRegistro', $fechaRegistro);
                    $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);    

                    $cmd->execute();

                    $registroAfectado = $cmd->rowCount();
                    if($registroAfectado>0){
                        return 1;    
                    }else{
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function actualizarPersona($idPersona,$idRol,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$usuario,$contrasenia,$fechaActualizacion) //$idRol,
        {   
            //idRol = :idRol,
            $sqlActualizarTesis = "UPDATE persona SET  idRol=:idRol, ci = :ci, primerNombre = :primerNombre, segundoNombre = :segundoNombre, primerApellido = :primerApellido, segundoApellido = :segundoApellido, telefono = :telefono, usuario = :usuario, contrasenia= :contrasenia, fechaActualizacion = :fechaActualizacion  
            WHERE idPersona = :idPersona;";

            try{
                    $cmd = $this->conexion->prepare($sqlActualizarTesis);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idPersona', $idPersona);
                   
                    $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':primerApellido', $primerApellido);
                    $cmd->bindParam(':segundoApellido', $segundoApellido);
                    $cmd->bindParam(':telefono', $telefono);
                    $cmd->bindParam(':usuario', $usuario);
                    $cmd->bindParam(':contrasenia', $contrasenia);
                    $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);

                   
                    // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
                    // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->execute();

                    // $ActualizarConsulta = $cmd->fetchAll();
    
                    // return $ActualizarConsulta;
            
                    return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        echo "ID ultimo: ".$this->conexion->lastInsertId();
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function actualizarPersonalTesis($idPersonalTesis,$idRolPersonalTesis,$ci,$primerNombre,$segundoNombre,$apellidoPaterno,$apellidoMaterno,$fotografia) //$idRol,
        {   
            //idRol = :idRol,
            $sqlActualizarTesis = "UPDATE personalTesis SET  idRolPersonalTesis=:idRolPersonalTesis, ci = :ci, primerNombre = :primerNombre, segundoNombre = :segundoNombre, apellidoPaterno = :apellidoPaterno, apellidoMaterno = :apellidoMaterno, fotografia = :fotografia  
            WHERE idPersonalTesis = :idPersonalTesis;";

            try{
                    $cmd = $this->conexion->prepare($sqlActualizarTesis);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idPersonalTesis', $idPersonalTesis);
                   
                    $cmd->bindParam(':idRolPersonalTesis', $idRolPersonalTesis);  
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':apellidoPaterno', $apellidoPaterno);
                    $cmd->bindParam(':apellidoMaterno', $apellidoMaterno);
                    $cmd->bindParam(':fotografia', $fotografia);


                   
                    // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
                    // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->execute();

                    // $ActualizarConsulta = $cmd->fetchAll();
    
                    // return $ActualizarConsulta;
            
                    return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        echo "ID ultimo: ".$this->conexion->lastInsertId();
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function

        public function actualizarPerfil($idPersona,$ci,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$telefono,$fotografia,$contrasenia,$fechaActualizacion) //$idRol,
        {   
            //idRol = :idRol,
            $sqlActualizarTesis = "UPDATE persona SET  ci = :ci, primerNombre = :primerNombre, segundoNombre = :segundoNombre, primerApellido = :primerApellido, segundoApellido = :segundoApellido, telefono = :telefono, fotografia= :fotografia, contrasenia= :contrasenia, fechaActualizacion = :fechaActualizacion  
            WHERE idPersona = :idPersona;";
            try{
                    $cmd = $this->conexion->prepare($sqlActualizarTesis);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idPersona', $idPersona);
                   
                    // $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':ci', $ci);
                    $cmd->bindParam(':primerNombre', $primerNombre);
                    $cmd->bindParam(':segundoNombre', $segundoNombre);
                    $cmd->bindParam(':primerApellido', $primerApellido);
                    $cmd->bindParam(':segundoApellido', $segundoApellido);
                    $cmd->bindParam(':telefono', $telefono);
                    $cmd->bindParam(':fotografia', $fotografia);
                    $cmd->bindParam(':contrasenia', $contrasenia);
                    $cmd->bindParam(':fechaActualizacion', $fechaActualizacion);
                   
                    // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
                    // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->execute();

                    // $ActualizarConsulta = $cmd->fetchAll();
    
                    // return $ActualizarConsulta;
            
                    return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        echo "ID ultimo: ".$this->conexion->lastInsertId();
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function inactivo($idPersona,$estado) //$idRol,
        {   
            //idRol = :idRol,
            $sqlActualizarTesis = "UPDATE persona SET activo = :estado  
            WHERE idPersona = :idPersona;";
            try{
                    $cmd = $this->conexion->prepare($sqlActualizarTesis);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idPersona', $idPersona);
                   
                    // $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':estado', $estado);
                   
                    // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
                    // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->execute();

                    // $ActualizarConsulta = $cmd->fetchAll();
    
                    // return $ActualizarConsulta;
            
                    return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        echo "ID ultimo: ".$this->conexion->lastInsertId();
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function


        public function inactivoPersonal($idPersonalTesis,$estado) //$idRol,
        {   
            //idRol = :idRol,
            $sqlActualizarTesis = "UPDATE personalTesis SET activo = :estado  
            WHERE idPersonalTesis = :idPersonalTesis;";
            try{
                    $cmd = $this->conexion->prepare($sqlActualizarTesis);
                    //$cmd->bindParam(':idUsuario', $idUsuario); el gestor genera la llave primaria AutoIncremental
                    $cmd->bindParam(':idPersonalTesis', $idPersonalTesis);
                   
                    // $cmd->bindParam(':idRol', $idRol);
                    $cmd->bindParam(':estado', $estado);
                   
                    // $cmd->bindParam(':fechaHoraRegistro', $fechaHoraRegistro);
                    // $cmd->bindParam(':idTipoTesis', $idTipoTesis);
                    $cmd->execute();

                    // $ActualizarConsulta = $cmd->fetchAll();
    
                    // return $ActualizarConsulta;
            
                    return 1;
                    $registroAfectado = $cmd->rowCount();
                     if($registroAfectado>0)
                    {
                        echo "ID ultimo: ".$this->conexion->lastInsertId();
                    }
                    else
                    {
                        return 0;
                    }

            }catch(PDOException $e){
                echo 'ERROR: No se logro realizar la nueva inserción - '.$e->getMesage();
                exit();
                return 0;
            }
        }//end function




    }

?>

